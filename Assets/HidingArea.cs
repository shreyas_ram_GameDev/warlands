using UnityEngine;
using DG.Tweening;
using System;

public class HidingArea : MonoBehaviour
{
    public static Action<bool> isPlayerHiding = delegate { };
    private void OnTriggerEnter(Collider other) {
        this.transform.GetComponent<MeshRenderer>().material.DOFade(0.5f, 1f);
        isPlayerHiding?.Invoke(true);
    }

    private void OnTriggerExit(Collider other) {
        this.transform.GetComponent<MeshRenderer>().material.DOFade(1f, 1f);
        isPlayerHiding?.Invoke(false);
    }

}
