using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Opsive.UltimateCharacterController.Items.Actions;

public class BowTrejectory : MonoBehaviour
{
    LineRenderer lineRenderer;

    ShootableWeapon shootableWeapon;

    [SerializeField] private Transform firePoint;

    public int numPointes = 50;

    public float timeBetweenPoints = 0.1f;

    public LayerMask Collidablelayer;

    private void Start()
    {
        lineRenderer = GetComponent<LineRenderer>();
        shootableWeapon = GetComponent<ShootableWeapon>();
    }

    private void Update()
    {
        //firePoint = shootableWeapon.m_SpawnedProjectile.transform;
        lineRenderer.positionCount = numPointes;
        List<Vector3> points = new List<Vector3>();
        Vector3 startPosition = firePoint.position;
        Vector3 startingVelocity = firePoint.up * shootableWeapon.ProjectileFireVelocityMagnitude;
        for (float i = 0; i < numPointes; i += timeBetweenPoints)
        {
            Vector3 newPoint = startPosition + i * startingVelocity;
            //projection points
            newPoint.y = startPosition.y + startingVelocity.y * i + Physics.gravity.y * 0.5f * i * i;
            points.Add(newPoint);
            if (Physics.OverlapSphere(newPoint, 2, Collidablelayer).Length > 0)
            {
                lineRenderer.positionCount = points.Count;
                break;
            }
        }

        lineRenderer.SetPositions(points.ToArray());
    }

}
