using UnityEngine;

public class ResourceManager : MonoBehaviour
{
	public static ResourceManager Instance;
    void Awake ( ) {
		if ( Instance == null ) {
			Instance = this;
			DontDestroyOnLoad ( this.gameObject );
		} else {
			Destroy ( this.gameObject );
		}
	}


    [Header("Visual Effects References")]
    public GameObject monsterSpawnFx;



    public void LoadResource()
    {
        Resources.Load(monsterSpawnFx.name, typeof(GameObject));
    }
}
