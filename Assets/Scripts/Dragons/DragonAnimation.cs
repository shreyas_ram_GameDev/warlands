﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragonAnimation : MonoBehaviour
{
    public List<AnimationClip> anim = new List<AnimationClip>();
    public Animator animator;
    // Start is called before the first frame update
    void Start()
    {
       animator = GetComponent<Animator>();
        StartAniamtion();
    }

    // Update is called once per frame
    public void StartAniamtion()
    {
        int rand = Random.Range(0, anim.Count);
        animator.Play(anim[rand].name);
        StartCoroutine(ChangeAniamtion());

    }

    public IEnumerator ChangeAniamtion()
    {
        yield return new WaitForSeconds(10);
        StartAniamtion();
    }
}
