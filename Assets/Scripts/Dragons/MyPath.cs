﻿using System.Collections;
using UnityEngine;
using Photon.Pun;
using System;

public class MyPath : MonoBehaviourPunCallbacks
{
    // public PathCreator pathCreator;
    // public PathFollower pathFollower;
    public Vector3 Target;
    public float speed;
    public float Distance;
    private int layerMask;
    public int Teamcode = 0;
    private bool _setposition;

    public float _photonID;
    [SerializeField] float tiltSpeed;
    Rigidbody rb;
    float zRot = 10;
    float yRot = 20; //steer amount

    public GameObject virtualControllerPlayer;
    public GameObject virtualControllerDragon;
    // public GameObject virtualControllerDragonAltitude;
    private Animator animator;

    public static Action OnDragonLandedEvent = delegate { };
    private void Awake() {
        animator = GetComponent<Animator>();
    }
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        layerMask = LayerMask.GetMask("Ground");
        _setposition = true;
        Distance = 10;
        _photonID = GetComponent<PhotonView>().ViewID;
        virtualControllerPlayer.SetActive(false);
        virtualControllerDragon.SetActive(true);


        Invoke("ActivateDescent", 10);
    }
 
    void FixedUpdate ()
    {

        SteerControl();
    }

    // Update is called once per frame
    void Update()
    {
        
        // if(turnRight)

            
        // else

        // if (Input.GetMouseButtonDown(0))
        // {
           
        //     if (PhotonNetwork.IsMasterClient)
        //     {
              
        //         GetToSpawnPointBlue();
        //         //if (PhotonNetwork.IsMasterClient)
        //         //{
        //         //    photonView.RPC("GetToSpawnPointBlue", RpcTarget.AllBuffered, target);
        //         //}
        //     }
        //     else if (MyGameManager.Instance.TeamCode == 2 && MyPlayerManager.Instance.isLeader == true)
        //     {
        //         GetToSpawnPointRed();
        //     }
           

        // }
        
        if (Target != new Vector3(0, 0, 0))
        {
            // NewTarget(teamcode);
            SetDistination();

        }
    }


    private void SteerControl()
    {
        float horizontal = ETCInput.GetAxis("HorizontalAlternate");
        float vertical = ETCInput.GetAxis("VerticalAlternate");
        float altitude = ETCInput.GetAxis("VerticalAltitude");
        // Debug.Log(vertical);
        rb.AddRelativeForce(horizontal * tiltSpeed,altitude * tiltSpeed, vertical * tiltSpeed);
        // animator.SetFloat("HorizontalMovement", horizontal);
        animator.SetFloat("VerticalMovement", vertical);

        if(horizontal > 0)
        {
            // transform.eulerAngles = new Vector3 (0, 0, 15);
            // zRot += 10*Time.deltaTime; // tile amount
            
            transform.Rotate (new Vector3 (0, yRot, 0) * Time.deltaTime);
        }

        if(horizontal < 0)
        {
            // zRot -= 10*Time.deltaTime;
            // transform.eulerAngles =  new Vector3 (0, 0, -15);
            transform.Rotate (new Vector3 (0, -yRot, 0) * Time.deltaTime);
        }
        // if(vertical > 0)
        // {
        //     // transform.eulerAngles = new Vector3 (0, 0, 15);
        //     // zRot += 10*Time.deltaTime; // tile amount
            
        //     transform.Rotate (new Vector3 (20, yRot, -zRot) * Time.deltaTime);
        // }

        // if(vertical < 0)
        // {
        //     // zRot -= 10*Time.deltaTime;
        //     // transform.eulerAngles =  new Vector3 (0, 0, -15);
        //     transform.Rotate (new Vector3 (-20, -yRot, zRot) * Time.deltaTime);
        // }

        if(horizontal == 0 || vertical == 0)
            transform.Rotate (new Vector3 (0, 0, 0));
    }



    private void ActivateDescent()
    {
        rb.useGravity = true;
    }
    [PunRPC]
    public void NewTarget(int teamcode, Vector3 _target)
    {
        
        Teamcode = teamcode;
        if (MyGameManager.Instance.TeamCode != teamcode) return;
        // this.pathCreator.enabled = false;
        // this.pathFollower.enabled = false;

        Target = _target;
    }

    public void SetDistination()
    {
        var Velocity = speed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, Target, Velocity);
        transform.LookAt(Target);
        Distance = Vector3.Distance(transform.position, Target);
        if (Distance <= 2)
        {
            if (_setposition == true)
            {
                _setposition = false;
                SetPlayerPos(Target, Teamcode);
                SpawnPlayers();
            }
        }
    }
    
    private void SetPlayerPos(Vector3 _target, int _teamcode)
    {
        if (MyGameManager.Instance.TeamCode != _teamcode) return;
            MyGameManager.Instance.SetPlayerPosition(_target, _teamcode);  

        photonView.RPC("NewTarget", RpcTarget.All, _teamcode, _target);
    }
   
    // public void GetToSpawnPointBlue()
    // {
       
    //     if (photonView.ViewID == 7)//photonView.ViewID == 10) 
    //     {
           
    //         if (Target != new Vector3(0, 0, 0)) return;
          
    //         RaycastHit hit;
    //         UIManager.Instance.TutorialText(2);

    //         // Raycast and hit to Move Player to Hit point
    //         if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, Mathf.Infinity, layerMask))
    //         {

                    
    //                 Teamcode = 1;
    //             this.Target = new Vector3(hit.point.x, hit.point.y + 10, hit.point.z);
    //             photonView.RPC("NewTarget", RpcTarget.All, Teamcode, hit.point);


    //         }
                     
    //     }
    // }
    // public void GetToSpawnPointRed()
    // {
    //     if (photonView.ViewID == 9)
    //     {
    //         if (Target != new Vector3(0, 0, 0)) return;
    //         UIManager.Instance.TutorialText(2);
    //         RaycastHit hit;
          
    //         // Raycast and hit to Move Player to Hit point
    //         if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, Mathf.Infinity, layerMask))
    //         {
                            
    //             Teamcode = 2;
    //             this.Target = new Vector3(hit.point.x, hit.point.y + 10, hit.point.z);
    //             photonView.RPC("NewTarget", RpcTarget.All, Teamcode, hit.point);

    //         }
    //     }
    // }

    public int GetTeamCode()
    {
        if(photonView.ViewID == 9)
        {
            return 2;
        }
        if (photonView.ViewID == 7)//photonView.ViewID == 10) 
        {
            return 1;
        }
        return 0;
    }

    private void OnCollisionEnter(Collision other) 
    {
        if(other.collider.CompareTag("Ground"))
        {
            OnDragonGrounded();
        }
    }
    // private void OnTriggerEnter(Collider other) 
    // {
    //     if(other.CompareTag("Ground"))
    //     {
    //         Debug.Log("Is Landing");
    //         animator.SetTrigger("isLanding");
    //     }
    // }
    public void OnDragonGrounded()
    {
        OnDragonLandedEvent?.Invoke();
        virtualControllerDragon.SetActive(false);
        Invoke("EnablePlayerJoystick", 1f);
        Target = this.transform.position;
        // TeamCode = 1;
        SetPlayerPos(Target, GetTeamCode());
        SpawnPlayers();
    }

    private void EnablePlayerJoystick()
    {
        virtualControllerPlayer.SetActive(true);
    }

    public void SpawnPlayers()
    {
        StartCoroutine(Disable());
       
    }

    public IEnumerator Disable()
    {
        yield return new WaitForSeconds(2f);
        
        Destroy(gameObject);
        // Destroy(pathCreator);

    }
}

