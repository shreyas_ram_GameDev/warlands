using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "MonsterType", menuName = "MonsterData/MonsterType", order = 1)]
public class MonsterType : ScriptableObject
{

    public GameObject monsterPrefab;
    public Material mat;
    public GameObject spawnFxPrefab;
    public int spawnId;



}
