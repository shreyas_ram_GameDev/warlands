﻿using System;
using System.Collections;
using System.Collections.Generic;
using Opsive.UltimateCharacterController.Traits;
using UnityEngine;

public class MyMonsterManager : MonoBehaviour
{
    CharacterHealth characterHealth;
    public SkinnedMeshRenderer meshRenderer;
    public Material defaultMat;
    public Material whiteMat;
    float maxHealth;

    private static int instanceId;
    public int thisMonsterId;
    // public int InstanceId{
    //     set { instanceId = value; }
    // }
    public static Action<int, GameObject> MonsterDisabledEvent = delegate { };

    private void Awake() 
    {
        characterHealth = GetComponent<CharacterHealth>();
    }
    private void Start() 
    {
        maxHealth = characterHealth.HealthValue;
    }
    public void ReduceHealthOnDeath() // respawns with reduced health
    {
        characterHealth.Heal(maxHealth * 0.5f);
    }
    private void OnDisable() 
    {
        MonsterDisabledEvent?.Invoke(thisMonsterId, this.gameObject);
    }


    public void OnDamageVisualization()
    {
        meshRenderer.material = whiteMat;
        Invoke("ResetMat", 0.2f);
    }
    public void OnDamageVisualization(float time)
    {
        meshRenderer.material = whiteMat;
        Invoke("ResetMat", time);
    }

    private void ResetMat()
    {
        meshRenderer.material = defaultMat;
    }


 
}
