using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterSpawnManager : MonoBehaviour
{
    public MonsterType _monster;
    public Transform[] spawnPoints;
    private int monsterDeathCount;
    public GameObject bossPrefab;
    public GameObject bossSpawnFx;

    private void Awake() {
        MyMonsterManager.MonsterDisabledEvent += HandleMonsterRespawnEvent;
    }

    private void OnDisable() {
        MyMonsterManager.MonsterDisabledEvent -= HandleMonsterRespawnEvent;
    }

    IEnumerator GetNewMonster(int monsterId, GameObject newMonster)
    {
        yield return new WaitForSeconds(8);
        // if(_monster.spawnFxPrefab!=null)
        if(newMonster != null)
        {
            GameObject currentEntityFx = Instantiate(_monster.spawnFxPrefab, spawnPoints[monsterId].position, Quaternion.Euler(-90,0,0));
            yield return new WaitForSeconds(2);
            newMonster.SetActive(true);
            newMonster.transform.position = spawnPoints[monsterId].position;
        }
    }

    private void HandleMonsterRespawnEvent(int monsterId, GameObject newMonster)
    {
        if(spawnPoints.Length > 0)
        {
            if(spawnPoints[monsterId] != null && spawnPoints[monsterId].gameObject.activeInHierarchy)
            {
                // StopCoroutine("GetNewMonster");
                StartCoroutine(GetNewMonster(monsterId, newMonster));
            }
        }
    }

    void Start()
    {
        // SpawnMonsters();
    }
    void SpawnMonsters()
    {
        int currentSpawnPointIndex = 0;

        for (int i = 0; i < spawnPoints.Length; i++)
        {
            // Creates an instance of the prefab at the current spawn point.
            GameObject currentEntity = Instantiate(_monster.monsterPrefab, spawnPoints[currentSpawnPointIndex].position, Quaternion.identity);
            GameObject currentEntityFx = Instantiate(_monster.spawnFxPrefab, spawnPoints[currentSpawnPointIndex].position, Quaternion.Euler(-90,0,0));
            currentEntity.transform.SetParent(spawnPoints[i].transform, true);
            // currentEntity.GetComponent<MyMonsterManager>().InstanceId = currentSpawnPointIndex;

            // instanceNumber++;
            currentSpawnPointIndex++;
        }
    }

    public void UnleashBoss()
    {
        monsterDeathCount++;
        if(monsterDeathCount == spawnPoints.Length - 1)
        {
            GameObject currentEntityFx = Instantiate(_monster.spawnFxPrefab, spawnPoints[spawnPoints.Length - 1].position, Quaternion.Euler(-90,0,0));
            bossPrefab.SetActive(true);
        }
    }


}
