using System.Collections;
using System.Collections.Generic;
using Opsive.UltimateCharacterController.Traits;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
public class MonsterSpawnPlatform : MonoBehaviour
{
    private CharacterHealth characterHealth;
    public Image platformHealthIndicator;
    public GameObject explosionFx;

    public GameObject aura;
    float platformHealth;
    private void Awake() 
    {
        characterHealth = GetComponent<CharacterHealth>();
        

    }
    void Start()
    {
        platformHealthIndicator.gameObject.SetActive(false);
        platformHealthIndicator.fillAmount = characterHealth.HealthValue;
    }

    public void DamagePlatform()
    {
        platformHealthIndicator.gameObject.SetActive(true);
        platformHealth = characterHealth.HealthValue;
        var fillAmount = platformHealth / 100;
        platformHealthIndicator.DOFillAmount(fillAmount, 0.2f);

        Invoke("ResetHealthBar", 2f);
    }

    void ResetHealthBar()
    {
        platformHealthIndicator.gameObject.SetActive(false);
    }

    public void OnDestroyed()
    {
        explosionFx.SetActive(true);
        
        aura.transform.DOScaleY(0, 2f).OnComplete(() => Destroy(this.gameObject, .1f));
    }
}
