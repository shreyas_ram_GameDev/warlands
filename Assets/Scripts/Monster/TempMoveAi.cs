﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Photon.Pun;
using Photon.Realtime;
using Opsive.UltimateCharacterController.Utility;
using Opsive.UltimateCharacterController.Game;
using Opsive.UltimateCharacterController.Traits;

public class TempMoveAi : MonoBehaviourPunCallbacks, IFreezable
{
    public enum State {
        idle,
        Roaming,
        Chasing,
        Attack,
        Formation,
        Retreat,
        Die,
    }

    [SerializeField] public State state = State.Roaming;
    [SerializeField] protected GameObject _bossMonster = null;

    private NavMeshAgent ai;
    private PhotonView pv;
    private Animator _monsterAnim;
    private Vector3 Randompos;
    private Vector3 m_center;

    public MonsterGroup monsterGroup;
    public Transform _spawnPosition;
    public float _roamingRadius;
    public string StarPickup = "MyStarPickupSpawn";
    public GameObject target;
    public float reachedDistance = 1f;
    public float _enemyforRadius = 2;

    [SerializeField] float attackRadius = 3;
    public Health health;

    // Start is called before the first frame update
    void Start()
    {
        ai = GetComponent<NavMeshAgent>();
        pv = GetComponent<PhotonView>();
        _monsterAnim = GetComponent<Animator>();
        m_center = transform.position;
        DetermineDestination();
        state = State.Roaming;
        health = GetComponent<Health>();
        monsterGroup = transform.GetComponentInParent<MonsterGroup>();
        
        
    }

    // Update is called once per frame
    void Update()
    {
        Movement();
        // Die();



    }

    public void UpdateState()
    {
        switch (state)
        {
            case State.idle:
                ai.speed = 0f;
                break;

            case State.Roaming:
                ai.speed = 1f;
                _monsterAnim.SetFloat("WalkSpeed", 0.7f);
                _monsterAnim.SetBool("Walk", true);
                _monsterAnim.SetBool("Attack", false);
                break;

            case State.Chasing:
                ai.speed = 6f;
                _monsterAnim.SetFloat("WalkSpeed", 1.5f);
                _monsterAnim.SetBool("Attack", false);
                _monsterAnim.SetBool("Walk", true);
                break;
            case State.Formation:
                ai.speed = 6f;
                _monsterAnim.SetFloat("WalkSpeed", 1.5f);
                _monsterAnim.SetBool("Attack", false);
                _monsterAnim.SetBool("Walk", true);
                break;

            case State.Attack:
                _monsterAnim.SetBool("Walk", false);
                
                break;

            case State.Retreat:
                break;

            case State.Die:
                break;

        }
    }

    /// <summary>
    /// Checks for monster movement
    /// </summary>
    public void Movement()
    {

        FindNearbyPlayer(_enemyforRadius);
        if (target != null)
        {
            CheckAndAttack();
        }
        else
        {
           
           
            ai.enabled = true;
            if (ai != null)
            {
                Randompos.y = transform.position.y;
                ai.SetDestination(Randompos);
                ai.updateRotation = true;
                state = State.Roaming;
                UpdateState();

            }
            else
            {
                ai = GetComponent<NavMeshAgent>();
            }

            if (Vector3.Distance(transform.position, Randompos) < reachedDistance)
            {
                DetermineDestination();
            }
        }
    }


    private void DetermineDestination()
    {

        var randomPosition = UnityEngine.Random.insideUnitSphere * _roamingRadius;
        randomPosition.y = 0;
        Randompos = (m_center + randomPosition);
    }

    private void FindNearbyPlayer(float radius)
    {
        if (target == null)
        {
            Collider[] col = Physics.OverlapSphere(transform.position, radius);
            foreach (Collider hit in col)
            {


                if (MathUtility.InLayerMask(hit.gameObject.layer, 1 << LayerManager.Character))
                {

                    target = hit.gameObject;

                }

            }
        }
        if (target != null)
        {
            if (Vector3.Distance(transform.position, target.transform.position) > _enemyforRadius)
            {
                if (state == State.Formation) return;
                target = null;

            }
            else
            {
                if (state == State.Formation)
                {
                    state = State.Chasing;
                    UpdateState();
                }
            }
        }



    }

    private void CheckAndAttack()
    {

      
        if (ai.enabled == true)
        {
            ai.SetDestination(target.transform.position);
        }

        if (Vector3.Distance(transform.position, target.transform.position) < attackRadius)
        {
           
            ai.enabled = false;
            Attack();
           

        }
        else if(Vector3.Distance(transform.position, target.transform.position) > 3.5f)
        {
            if (state != State.Formation)
            {
                state = State.Chasing;
                UpdateState();
            }
          
            ai.enabled = true;
        }

    }

    private void Attack()
    {
        state = State.Attack;
        _monsterAnim.SetBool("Attack", true);
    }

    public void Death()
    {
        state = State.Die;
        // this.gameObject.SetActive(false);
        _monsterAnim.SetBool("isDead",true);

    }
    private void Die()
    {
        if (!photonView.IsMine) return;
        if (health.Value <= 0)
        {
            if (_bossMonster != null)
            {
                
                // monsterGroup._dead = true;
                if (target.transform.root.GetComponent<MyPlayerManager>() != null)
                {
                    monsterGroup.OnDeath(target.transform.root.GetComponent<MyPlayerManager>().TeamCode);

                }
                else if ((target.transform.root.GetComponent<MyBotManager>() != null))
                {

                    monsterGroup.OnDeath(target.transform.root.GetComponent<MyBotManager>().TeamCode);

                }
            }
            else
            {
                monsterGroup._monsters.Remove(this.transform);
            }
           
            Destroy(gameObject);
        }
    }

    public void SpawnStar()
    {
        PhotonNetwork.Instantiate(StarPickup, new Vector3(transform.position.x, transform.position.y, transform.position.z), Quaternion.identity);
    }


    public void FreezeCo()
    {
        StartCoroutine(Freeze());
    }

    IEnumerator Freeze()
    {
        _monsterAnim.speed = 0;
        this.enabled = false;
        ai.destination = transform.position;
        GetComponent<MyMonsterManager>().OnDamageVisualization(2);
        yield return new WaitForSeconds(2);
        _monsterAnim.speed = 1;
        this.enabled = true;
    }
}
