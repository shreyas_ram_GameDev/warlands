﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterGroup : MonoBehaviour
{
    public string _landtype = null;
    public List<Transform> _monsters;
    public GameObject _bossMonster;
    public GameObject[] _spawnOnDeath;
    private TempMoveAi _bossAI;

    public bool _dead;
    public bool _groupInformed;

    public void Update()
    {
        if (_bossAI == null && _dead == false)
        {
            _bossAI = _bossMonster.GetComponent<TempMoveAi>();
        }
        else
        {
            if (_groupInformed == false)
            {
                FormGroup();
            }
        }
      
    }

    public void FormGroup()
    {
        if (_bossAI.state == TempMoveAi.State.Attack)
        {
            var target = _bossAI.target;
            foreach (var monster in _monsters)
            {
               
                if (monster.GetComponent<TempMoveAi>().target == null)
                {
                    monster.GetComponent<TempMoveAi>().target = target;
                    monster.GetComponent<TempMoveAi>().state =  TempMoveAi.State.Formation;
                    monster.GetComponent<TempMoveAi>().UpdateState();

                }
            }  
            
        }
    }

    public void OnDeath(float teamcode)
    {
        OnDeathAbility(teamcode, _landtype);
    }

    public void OnDeathAbility(float teamcode, string abilityType)
    {
        AblityHandler.Instance.UpdateAbility(teamcode, abilityType);
    }
}
