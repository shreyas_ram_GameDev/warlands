﻿using UnityEngine;
using Opsive.UltimateCharacterController.Utility;
using Opsive.UltimateCharacterController.Game;
using Opsive.UltimateCharacterController.Traits;
using static MyBotState;

public class MonsterAttack : MonoBehaviour
{
    public float _damage;
    public Health health;
    public bool isMonster;
    [SerializeField] MyBotManager myBotManager = null;
    [SerializeField] MyBotState botState = null;

    private Collider myCollider;

    void Start()
    {
        myCollider = GetComponent<Collider>();
        if (isMonster == false)
        {
            myBotManager = transform.root.GetComponent<MyBotManager>();
            botState = transform.root.GetComponent<MyBotState>();
        }
    }
    private void OnTriggerEnter(Collider collider)
    {
        if (collider == myCollider) return;
       
        if (isMonster == true)
        {
            if (MathUtility.InLayerMask(collider.gameObject.layer, 1 << LayerManager.Character))
            {
                
                health = collider.gameObject.GetComponentInParent<Health>();
                health.Damage(_damage);
               

            }
        }
        else if (MathUtility.InLayerMask(collider.gameObject.layer, 1 << LayerManager.Character) || MathUtility.InLayerMask(collider.gameObject.layer, 1 << LayerManager.Enemy))
        {
            if (botState.CurrentGameState != State.Attack) return;
            if (collider.transform.root.GetComponent<MyPlayerManager>() != null)
            {

                if ((collider.transform.root.GetComponent<MyPlayerManager>().TeamCode != myBotManager.TeamCode))
                {
                    health = collider.gameObject.GetComponentInParent<Health>();
                    health.Damage(_damage);
                   
                }

            }
            else if ((collider.transform.root.GetComponent<MyBotManager>() != null))
            {

                if ((collider.transform.root.GetComponent<MyBotManager>().TeamCode != myBotManager.TeamCode))
                {
                   
                    health = collider.gameObject.GetComponentInParent<Health>();
                    health.Damage(_damage);
                   
                }

            }
            else
            {
                
                health = collider.gameObject.GetComponentInParent<Health>();
                health.Damage(_damage);
               
                
            }
                
          
        }
             
    }


}
