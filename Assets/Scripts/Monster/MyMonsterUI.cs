﻿using Opsive.UltimateCharacterController.Traits;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;
public class MyMonsterUI : MonoBehaviour
{
    [Tooltip("UI Slider to display Player's Health")]
    [SerializeField]
    public Image playerHealthSlider;

    [Tooltip("UI Image to display Player's Team")]
    [SerializeField] public Image playerTeam;


    [Tooltip("UI Image for minimap team indicator")]
    [SerializeField] public SpriteRenderer miniMapTeam;

    [SerializeField] private float _health;
    [SerializeField] private float _totalHealth;
    float tempHealth;
    public TextMeshProUGUI damagePopUpText;
    public CharacterHealth characterHealth;

    Vector3 damagePopUpTextInitPosition;
    public GameObject floatTextPrefab;

    void Awake()
    {
        playerHealthSlider.gameObject.SetActive(false);
        characterHealth = transform.parent.parent.GetComponent<CharacterHealth>();

    }

    private void Start() {
        tempHealth = characterHealth.HealthValue;
        damagePopUpTextInitPosition = damagePopUpText.rectTransform.position;
    }


    public void Health()
    {
        playerHealthSlider.gameObject.SetActive(true);
        _health = characterHealth.HealthValue;
        playerHealthSlider.fillAmount = _health / _totalHealth;

        if (_health > 0)
            DamagePopUp(_health);
    }


    void DamagePopUp(float currentHealth)
    {
        float damageAmount = tempHealth - currentHealth;
        tempHealth = currentHealth;

        var floatTextGo = Instantiate(floatTextPrefab, transform.position, Quaternion.identity);
        floatTextGo.GetComponent<TextMesh>().text = damageAmount.ToString();

        // damagePopUpText.text = "-" + damageAmount.ToString();
        // damagePopUpText.transform.DOMoveY(1, 0.2f);
        // damagePopUpText.DOFade(0, 0.2f);

        Invoke("HideHealthBar", 1f);
    }
    private void HideHealthBar()
    {
        playerHealthSlider.gameObject.SetActive(false);
    }

}

