﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Opsive.UltimateCharacterController.Traits;
using Opsive.UltimateCharacterController.Items.Actions.Magic.ImpactActions;

public class MyHeal : ImpactAction
{
    [Tooltip("The amount that should be added to the Health component.")]
    [SerializeField] protected float m_Amount = 10;
    [Tooltip("Should the subsequent Impact Actions be interrupted if the Health component doesn't exist?")]
    [SerializeField] protected bool m_InterruptImpactOnNullHealth = true;

    public float Amount { get { return m_Amount; } set { m_Amount = value; } }
    public bool InterruptImpactOnNullHealth { get { return m_InterruptImpactOnNullHealth; } set { m_InterruptImpactOnNullHealth = value; } }

    protected override void ImpactInternal(uint castID, GameObject source, GameObject target, RaycastHit hit)
    {
        Debug.Log("1");
        var health = target.GetComponent<Health>();
        Debug.Log("2");
        if (health != null)
        {
            health.Heal(m_Amount);
            Debug.Log("3");
        }
    }
}
