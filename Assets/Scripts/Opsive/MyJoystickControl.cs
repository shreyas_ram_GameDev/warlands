﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Opsive.Shared.Input;
using HedgehogTeam.EasyTouch;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Opsive.Shared.Events;
using Opsive.Shared.Game;
using Opsive.Shared.StateSystem;
using Opsive.Shared.Utility;

public  class MyJoystickControl : Opsive.Shared.Input.VirtualControls.VirtualAxis

{
   
    public override float GetAxis(string name)
    {
        if (name == m_HorizontalInputName)
        {

           // Debug.Log(ETCInput.GetAxis("Horizontal"));
            return ETCInput.GetAxis("Horizontal");
        }
        else
        {
            return ETCInput.GetAxis("Vertical");
        }

       
    }

   
}
