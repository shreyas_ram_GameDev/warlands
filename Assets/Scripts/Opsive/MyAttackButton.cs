﻿
using UnityEngine;
using Photon.Pun;
using Opsive.UltimateCharacterController.Items.Actions;
using UnityEngine.UI;
using DG.Tweening;


public class MyAttackButton : MonoBehaviour
{
    private Image _buttonImage;
    private Button button;

    public bool _buttonIsActive;


    public int _itemSet;
    public float _buttonDisableAmount;
    public MyVirtualButton virtualButton;
    public MyItemEquip itemEquip;

    private string Fire4 = "Fire4";
    private string Fire2 = "Fire2";
    private string Fire3 = "Fire3";


    public void Awake()
    {
        _buttonIsActive = false;
        itemEquip = GetComponentInParent<MyItemEquip>();

        _buttonImage = GetComponent<Image>();
      
        button = GetComponent<Button>();
        button.onClick.AddListener(() =>  Disable());


    }

    public void Start()
    {
        virtualButton.enabled = false;
        _buttonImage.enabled = false;
        button.interactable = false;
    }

    public void ButtonName()
    {
        if (_itemSet == 1)
        {
            virtualButton.m_ButtonName = Fire3;
            virtualButton.RegisterButton(Fire3);
            itemEquip._inputNames.Add(Fire3);
        }
        else if (_itemSet == 2)
        {
            virtualButton.m_ButtonName = Fire4;
            virtualButton.RegisterButton(Fire4);
            itemEquip._inputNames.Add(Fire4);
        }
        else if (_itemSet == 3)
        {
            virtualButton.m_ButtonName = Fire2;
            virtualButton.RegisterButton(Fire2);
            itemEquip._inputNames.Add(Fire2);
        }
      
    }
    public void Equip()
    {
        if (_itemSet == 1)
        {
            MyPunCallbacks.Instance.OnHeal();
        }
       MyPunCallbacks.Instance.Equip(_itemSet);
       MyPunCallbacks.Instance.useAbility.StartAbility();

    }

    private void Disable()
    {
        //    if (usableItem == null)
        //    {

        //        usableItem = FindObjectOfType<UsableItem>();
        //        Debug.Log(usableItem);
        //    }

        //if (Time.time > usableItem.m_NextAllowedUseTime)
        //{
        //    _buttonImage.fillAmount = 0;
        //    Equip();
        //    StartFilling();
        //}

        if (button.interactable == true)
        {
            button.interactable = false;
            Invoke("ActivateButton", _buttonDisableAmount);
            _buttonImage.fillAmount = 0;
            Equip();
            StartFilling();
        }


    }

    private void ActivateButton()
    {
        button.interactable = true;
    }

    public void StartFilling()
    {       
        //_buttonImage.DOFillAmount(1, 5f);
        _buttonImage.DOFillAmount(1, _buttonDisableAmount);

    }
}







