﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HedgehogTeam.EasyTouch;
using Opsive.Shared.Input;
using HedgehogTeam;
using Opsive.Shared.Events;
using Opsive.Shared.Game;

using Opsive.Shared.StateSystem;

using Opsive.Shared.Utility;

using UnityEngine.EventSystems;


public class Mytouch : Opsive.Shared.Input.PlayerInput
{
    public Vector2 position;
   

    // Subscribe to events

    void OnEnable()
    {
       
        EasyTouch.On_TouchDown += OnTouchDown;
        EasyTouch.On_TouchUp += OnTouchUp;

    }
    // Unsubscribe
    void OnDisable()
    {
      
        EasyTouch.On_TouchDown -= OnTouchDown;
        EasyTouch.On_TouchUp -= OnTouchUp;
    }

  
    private void OnTouchDown(Gesture gesture)
    {
        _canWalk = true;
    }
    private void OnTouchUp(Gesture gesture)
    {
        _canWalk = false;
    }
   
    public override bool OnTapInternal() 
    {
        return _canWalk;
    }

   

   



}