﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.AI;

public class BotManager : MonoBehaviourPunCallbacks
{
    [SerializeField] private string _playerBot;
    public List<GameObject> _botListBlue = new List<GameObject>();
    public List<GameObject> _botListRed = new List<GameObject>();

    public Transform[] DifferentZones;

    public Transform SpawnLocation;
    public Transform RedBotsSpawnPoint;

    public Transform RespawnPointBlue;
    public Transform RespawnPointRed;

    public static BotManager Instance;
    public int _botCount;


    private string blueBot = "BlueBot";
    private string redBot = "RedBot";


    private void Awake()
    {
        
        {
            Instance = this;
        }
    }
    

    public void AddBot(int a)
    {
        if (PhotonNetwork.IsMasterClient)
        {
            _botCount += a;
            UIManager.Instance.PlayerCount();
            SpawnBot();
            
        }
    }

    public void SpawnBot()
    {
        
        Vector3 offset = new Vector3(UnityEngine.Random.Range(-20, 20), 0, UnityEngine.Random.Range(-15, 15));
        GameObject bot =  PhotonNetwork.Instantiate(_playerBot, SpawnLocation.position + offset, SpawnLocation.rotation);
        if (MyGameManager.Instance.AllPlayerCounts % 2 == 0)
        {
            photonView.RPC("Addbot", RpcTarget.OthersBuffered, 2, bot.GetComponent<PhotonView>().ViewID);
            _botListRed.Add(bot);
            bot.name = redBot;
// 
            // bot.layer = LayerMask.NameToLayer(TeamManager.TeamLayer.TeamRed.ToString());


        }
        else if (MyGameManager.Instance.AllPlayerCounts % 2 != 0)
        {
            photonView.RPC("Addbot", RpcTarget.OthersBuffered, 1, bot.GetComponent<PhotonView>().ViewID);
            _botListBlue.Add(bot);
            bot.name = blueBot;
            // bot.layer = LayerMask.NameToLayer(TeamManager.TeamLayer.TeamBlue.ToString());

        }
        return;
       
    }

    [PunRPC]
    public void Addbot(int _teamcode, int _id)
    {
        if (_teamcode == 1)
        {
            var bot = PhotonView.Find(_id).gameObject;
            _botListBlue.Add(bot);

          
        }
        else if(_teamcode == 2)
        {
            var bot = PhotonView.Find(_id).gameObject;
            _botListRed.Add(bot);


        }
    }

    public void RpcTeamCheck()
    {
        if (!PhotonNetwork.IsMasterClient) return;
        foreach (var bot in _botListBlue)
        {
            photonView.RPC("SetTeamCheck", RpcTarget.Others, bot.GetComponent<MyBotManager>().TeamCode);

        }
        foreach (var bot in _botListRed)
       {
            photonView.RPC("SetTeamCheck", RpcTarget.Others, bot.GetComponent<MyBotManager>().TeamCode);

        }


    }
    

    [PunRPC]
    public void SetTeamCheck(int teamcode)
    {

        if (teamcode == 1)
        {
            foreach (var bots in _botListBlue)
            {
                bots.GetComponent<MyBotManager>().UpdaeteColor(teamcode);

            }
        }
        else if(teamcode == 2)
        {
            foreach (var bots in _botListRed)
            {
                bots.GetComponent<MyBotManager>().UpdaeteColor(teamcode);


            }
        }
    }

    public void SetBotsPosition(Vector3 _target, int _teamcdoe)
    {
        photonView.RPC("BotPositon", RpcTarget.Others, _target, _teamcdoe);
    }

    [PunRPC]
    public void BotPositon(Vector3 target , int _teamcode)
    {
        if (_teamcode == 1)
        {
            foreach (var bots in BotManager.Instance._botListBlue)
            {
               
                bots.GetComponent<NavMeshAgent>().enabled = false;
                bots.transform.position = target;
                bots.GetComponent<NavMeshAgent>().enabled = true;


            }
        }
        else if(_teamcode == 2)
        {
            foreach (var bots in BotManager.Instance._botListRed)
            {

                bots.GetComponent<NavMeshAgent>().enabled = false;
                bots.transform.position = target;
                bots.GetComponent<NavMeshAgent>().enabled = true;


            }

        }

    }
   
}

