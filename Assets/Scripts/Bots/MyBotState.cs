﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Photon.Pun;
using System;
using UnityEngine.AI;

public class MyBotState : MonoBehaviourPunCallbacks, IFreezable
{
    public enum State
    {
        Fall,
        idle,
        Roaming,
        Searching,
        Chasing,
        Attack,
        Formation,
        Retreat,
        Die,
    }
    [System.Serializable]
    public class EventGameState : UnityEvent<State, State> { }

    public EventGameState OnGameStateChange;
    [Header("Game State")]
    [SerializeField] public State _currentGameState;
    public State CurrentGameState { get { return _currentGameState; } private set { _currentGameState = value; } }
    public State _nextGameState;

    public int idle;
    private int walk;
    private int run;
    private int attack;
    private int die;

    private Animator animator;
    private NavMeshAgent agent;

    public override void OnEnable()
    {
        MyGameManager.SpawnEvent += PlayerSpawned;
    }

    private void Start()
    {
        animator = GetComponent<Animator>();
        agent = GetComponent<NavMeshAgent>();
        if (!photonView.IsMine) return;
        int a = UnityEngine.Random.Range(0, 2);
        StartState(a);

        //idle = Animator.StringToHash("IsIdle1");
        //walk = Animator.StringToHash("IsWalking1");
        //run = Animator.StringToHash("IsRunning1");
        //attack = Animator.StringToHash("Attack");
        //die = Animator.StringToHash("Die");

      

    }

    private void PlayerSpawned()
    {
        _nextGameState = State.Searching;
        UpdateState();
    }
   


   

    public void UpdateState()
    {
        State previousGameState = _currentGameState;
        _currentGameState = _nextGameState;

        switch (_currentGameState)
        {
            case State.Fall:

                break;

            case State.idle:
                animator.SetBool("IsIdle", true);
                break;

            case State.Roaming:
                animator.SetBool("IsIdle", false);
                animator.SetBool("IsWalking", true);
                animator.SetBool("IsRunning", false);

                break;
            case State.Searching:
                animator.SetBool("IsIdle", false);
                animator.SetBool("IsWalking", true);
                animator.SetBool("IsRunning", false);
                agent.speed = 2.5f;
                break;

            case State.Chasing:
                animator.SetBool("IsWalking", false);
                animator.SetBool("IsRunning", true);
                agent.speed = 5;

                break;
          

            case State.Attack:
                animator.SetTrigger("Attack");
                animator.SetBool("IsWalking", false);
                animator.SetBool("IsRunning", false);

                break;


            case State.Die:
                animator.SetBool("IsWalking", false);
                animator.SetBool("IsRunning", false);
                animator.SetTrigger("Die");
                break;

        }
        OnGameStateChange.Invoke(_currentGameState, previousGameState);
    }



   

    private void StartState(int a)
    {

        if (a == 0)
        {
            _nextGameState = State.idle;
            UpdateState();
        }
        else if(a == 1)
        {
            _nextGameState = State.Roaming;
            UpdateState();
        }

      
    }

    //add separate component containg below implementation of Ifreezable and add this onlt to enemybots as they are bifurcated during run time.
    public void FreezeCo()
    {
        StartCoroutine(Freeze());
    }
    IEnumerator Freeze()
    {
        animator.speed = 0;
        this.enabled = false;
        agent.destination = transform.position;
        // GetComponent<MyMonsterManager>().OnDamageVisualization(2);
        yield return new WaitForSeconds(2);
        animator.speed = 1;
        this.enabled = true;
    }

    public override void OnDisable()
    {
        MyGameManager.SpawnEvent -= PlayerSpawned;
    }



}
