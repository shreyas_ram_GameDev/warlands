﻿using Opsive.UltimateCharacterController.Character;
using Opsive.UltimateCharacterController;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Opsive.UltimateCharacterController.Character.Abilities.Items;
using Opsive.UltimateCharacterController.Character.Abilities;
using Opsive.UltimateCharacterController.Character.Abilities.AI;
using UnityEngine.AI;
using static MyBotState;
using Opsive.UltimateCharacterController.Utility;
using Opsive.UltimateCharacterController.Game;
using Photon.Pun;

public class BotMovement : MonoBehaviourPunCallbacks
{
    [SerializeField] MyBotManager myBotManager = null;
    [SerializeField] MyBotState botState = null;

    private Collider myCollider;
    private Collider[] col;
    public Vector3 Randompos;
    public Vector3 m_center;
    private float reachedDistance = 1f;

    public int[] layers = new int[] { LayerManager.Character, LayerManager.Enemy };
    private Vector3 ZoneTarget;
    public Transform target;
    public float _roamingRadius;
    public float _enemyforRadius;
    public NavMeshAgent agent;

    void Start()
    {
        if (!photonView.IsMine) return;
        myBotManager = GetComponent<MyBotManager>();
        botState = GetComponent<MyBotState>();
        agent = GetComponent<NavMeshAgent>();
        myCollider = GetComponentInChildren<Collider>();
        m_center = transform.position;
        DetermineDestination();
        botState.OnGameStateChange.AddListener(HandleGameStateChange);
        HandleGameStateChange(botState.CurrentGameState, State.Roaming);


    }


    public void HandleGameStateChange(State curState, State prevState)
    {
        if (curState == State.Searching)
        {
            SelectZone();
        }
    }

    private void Update()
    {
        if (!photonView.IsMine) return;
        FindNearbyPlayer(_enemyforRadius);
        if (target != null)
        {
            CheckAndAttack();
        }
        else
        {
           
            RandomRome();
            if (botState.CurrentGameState == State.Chasing | botState.CurrentGameState == State.Attack)
            {
                botState._nextGameState = State.Searching;
                botState.UpdateState();
            }
           
        }
      

    }
    /// <summary>
    /// Decides random position inside a sphere
    /// </summary>
    public void RandomRome()
    {
        if (botState.CurrentGameState == State.Roaming || botState.CurrentGameState == State.Searching)

        {
            agent.enabled = true;
            Randompos.y = transform.position.y;
            agent.SetDestination(Randompos);
            agent.updateRotation = true;
            if (Vector3.Distance(transform.position, Randompos) < reachedDistance)
            {
                DetermineDestination();
            }
        }
      
        
    }

    public void DetermineDestination()
    {

        var randomPosition = UnityEngine.Random.insideUnitSphere * _roamingRadius;
        randomPosition.y = 0;
        Randompos = (m_center + randomPosition);
    }

    /// <summary>
    /// Selects which zone to go to randomly
    /// </summary>
    private void SelectZone()
    {

        int rand = Random.Range(0, BotManager.Instance.DifferentZones.Length);
        Randompos = BotManager.Instance.DifferentZones[rand].position;
        ZoneTarget = BotManager.Instance.DifferentZones[rand].position;

    }

    private void FindNearbyPlayer(float radius)
    {
        if (!photonView.IsMine) return;
        if (myBotManager.isDead == true) return;
        if (botState.CurrentGameState == State.Searching)
        {
            if (target == null)
            {
                col = Physics.OverlapSphere(transform.position, radius, 1 << LayerManager.Character | 1 << LayerManager.Enemy);
                foreach (Collider hit in col)
                {

                    if (hit == myCollider) return;

                    if (MathUtility.InLayerMask(hit.gameObject.layer, 1 << LayerManager.Character) || MathUtility.InLayerMask(hit.gameObject.layer, 1 << LayerManager.Enemy))
                    {
                        if (hit.transform.root.GetComponent<MyPlayerManager>() != null)
                        {

                            if ((hit.transform.root.GetComponent<MyPlayerManager>().TeamCode == myBotManager.TeamCode))
                            {

                                //target = null;
                            }
                            else
                            {
                                target = hit.transform;
                                botState._nextGameState = State.Chasing;
                                botState.UpdateState();
                            }

                        }
                        else if ((hit.transform.root.GetComponent<MyBotManager>() != null))
                        {

                            if ((hit.transform.root.GetComponent<MyBotManager>().TeamCode == myBotManager.TeamCode))
                            {
                                //target = null;
                            }
                            else
                            {
                                target = hit.transform;
                                botState._nextGameState = State.Chasing;
                                botState.UpdateState();

                            }
                        }
                        else
                        {
                            target = hit.transform;
                            botState._nextGameState = State.Chasing;
                            botState.UpdateState();
                        }

                    }

                }
            }

        }
        else if (botState._currentGameState == State.Chasing)
        {
            RemoveTarget();
        }
       
      
    }



    private void CheckAndAttack()
    {
        if (!photonView.IsMine) return;

        if (agent.enabled == true)
        {
            agent.SetDestination(target.transform.position);
        }

        if (Vector3.Distance(transform.position, target.transform.position) < 3)
        {
            agent.transform.LookAt(target);
            agent.enabled = false;
            Attack();


        }
        else if (Vector3.Distance(transform.position, target.transform.position) > 3.5f)
        {
            botState._nextGameState = State.Chasing;
            botState.UpdateState();
            agent.enabled = true;



        }
    }
    private void RemoveTarget()
    {
        if (!photonView.IsMine) return;
        if (target == null) return;
        
        if (Vector3.Distance(transform.position, target.transform.position) > (_enemyforRadius))
        {
            

            target = null;
            botState._nextGameState = State.Searching;
            botState.UpdateState();
        }
    }

    private void Attack()
    {
        if (!photonView.IsMine) return;
        botState._nextGameState = State.Attack;
        botState.UpdateState();
    }
}
