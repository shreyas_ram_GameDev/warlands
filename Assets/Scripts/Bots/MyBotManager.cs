﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ExitGames.Client.Photon;
using Photon.Realtime;
using Photon.Pun.UtilityScripts;
using Photon.Pun;
using System;
using Opsive.UltimateCharacterController.Traits;
using static MyBotState;
using Opsive.UltimateCharacterController.Game;
using Opsive.UltimateCharacterController.Character;

public class MyBotManager : MonoBehaviourPunCallbacks
{
    [SerializeField] MyBotState botState = null;
    private BotMovement botMovement;
    protected UltimateCharacterLocomotion characterLocomotion;
    protected UltimateCharacterLocomotionHandler locomotionHandler;

    public int TeamCode;

    public bool isDead;
    public Health health;
    public MyBotUI botUI;
    public bool zoneLeft;

    public void Start()
    {
        GetComponent<UltimateCharacterLocomotion>().enabled = false;
        GetComponent<UltimateCharacterLocomotionHandler>().enabled = false;

        botState = GetComponent<MyBotState>();
        botMovement = GetComponent<BotMovement>();
        botUI = GetComponentInChildren<MyBotUI>();
        health = GetComponent<Health>();      
        JoinTeam();

    }

    private void Update()
    {
        
        Die();
    }

    [PunRPC]
    private void JoinTeam()
    {

        if (MyGameManager.Instance.AllPlayerCounts % 2 == 0)
        {
            TeamCode = 2;
            botUI.playerTeam.color = Color.red;
            botUI.miniMapTeam.color = Color.red;
        }
        else if(MyGameManager.Instance.AllPlayerCounts % 2 != 0)
        {
            TeamCode = 1;
            botUI.playerTeam.color = Color.blue;
            botUI.miniMapTeam.color = Color.blue;
        }
      
    }
    public void UpdaeteColor(int teamcode)
    {
        if (teamcode == 1)
        {
            TeamCode = 1;
            botUI.playerTeam.color = Color.blue;
            botUI.miniMapTeam.color = Color.blue;
        }
        else if(teamcode == 2)
        {
            TeamCode = 2;
            botUI.playerTeam.color = Color.red;
            botUI.miniMapTeam.color = Color.red;
        }
    }

    private void OnTriggerEnter(Collider collider)
    {
        if (!photonView.IsMine) return;

        if (collider.gameObject.layer == LayerManager.Enemy)
        {
            
            botUI.Health();
        }
        else if (collider.gameObject.layer == LayerManager.Character)
        {
            if (collider.gameObject.transform.root.GetComponent<MyPlayerManager>() != null)
            {
                if (collider.gameObject.transform.root.GetComponent<MyPlayerManager>().TeamCode != TeamCode)
                {
                    botUI.Health();
                }
            }

            else if(collider.gameObject.transform.root.GetComponent<MyBotManager>() != null)
            {
                if (collider.gameObject.transform.root.GetComponent<MyBotManager>().TeamCode != TeamCode)
                {
                    botUI.Health();
                }
            }
          
        }

       

    }

    private void Die()
    {
        if (photonView.IsMine)
        {
            if (isDead != true)
            {
                if (health.Value <= 0)
                {
                    isDead = true;
                    botMovement.agent.enabled = false;
                    botMovement.target = null;
                    botState._nextGameState = State.Die;
                    botState.UpdateState();
                    StartCoroutine(Respawner());



                }
            }
        }


    }


    public IEnumerator Respawner()
    {
        yield return new WaitForSeconds(2.8f);
        botState._nextGameState = State.idle;
        botState.UpdateState();
        if (TeamCode == 1)
            transform.position = BotManager.Instance.RespawnPointBlue.position;
        else
            transform.position = BotManager.Instance.RespawnPointRed.position;
        Invoke("ResetValues", 4);
        


    }

    public void ResetValues()
    {
       
        botMovement.agent.enabled = true;
        botState._nextGameState = State.Searching;
        botState.UpdateState();
        health.Heal(100);
        botUI.Health();
        isDead = false;
    }


    public void ZoneLeftDamage()
    {
        if (!photonView.IsMine) return;
        if(ShrinkingZoneManager.Instance._canSpwnZone == true) return;
        health.Damage(20f);
        botMovement.DetermineDestination();
        StartCoroutine(Damage());


    }

    private IEnumerator Damage()
    {

        if (zoneLeft == false) yield break;
        yield return new WaitForSeconds(4);
        ZoneLeftDamage();

    }


}
