﻿using Opsive.UltimateCharacterController.Traits;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MyBotUI : MonoBehaviour
{

    [Tooltip("UI Text to display Player's Name")]
    [SerializeField]
    private Text playerNameText;


    [Tooltip("UI Slider to display Player's Health")]
    [SerializeField]
    public Image playerHealthSlider;

    [Tooltip("UI Image to display Player's Team")]
    [SerializeField] public Image playerTeam;


    [Tooltip("UI Image for minimap team indicator")]
    [SerializeField] public SpriteRenderer miniMapTeam;

    [SerializeField] public float _health;

    public CharacterHealth characterHealth;

    void Awake()
    {
       
        characterHealth = transform.parent.parent.GetComponent<CharacterHealth>(); ;

    }

   

    public void Health()
    {

        _health = characterHealth.HealthValue;
        playerHealthSlider.fillAmount = _health / 100;
    }

}
