﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.SceneManagement;

public static class EditorSceneLoader
{
    // % = ctrl/cmd
    // # = shift
    // & = alt
    //Example:
    [MenuItem("Scenes/Load MainMenu %1")] // ctrl/cmd + 1
    public static void LoadMainMenuScene() { openScene(EditorScenes.MainMenu); }

    [MenuItem("Scenes/Load TestScene")]
    public static void LoadTestScene() { openScene(EditorScenes.TestScene); }


    [MenuItem("Scenes/Load Test Arena")]
    public static void LoadTestArena() { openScene(EditorScenes.UccDemoForTesting); }

    // [MenuItem("Scenes/Load GlobalMatchWaitRoom")]
    // public static void LoadGlobalMatchWaitRoom() { openScene(Scenes.GlobalMatchWaitRoom); }

    // [MenuItem("Scenes/Load VsFriendsRoom")]
    // public static void LoadVsFriendsRoom() { openScene(Scenes.VsFriendsRoom); }
    // [MenuItem("Scenes/Load Toss")]
    // public static void Toss() { openScene(Scenes.Toss); }
    // [MenuItem("Scenes/Load GameScreen")]
    // public static void LoadGameScreen() { openScene(Scenes.GameScreen); }
    // [MenuItem("Scenes/Load Friends")]
    // public static void LoadFriends() { openScene(Scenes.Friends); }
    // [MenuItem("Scenes/Load SinglePlayerProgressPage")]
    // public static void LoadSinglePlayerProgressPage() { openScene(Scenes.SinglePlayerProgressPage); }
    // [MenuItem("Scenes/Load TopMenuBar")]
    // public static void LoadTopMenuBar() { openScene(Scenes.TopMenuBar); }
    // [MenuItem("Scenes/Load Tutorial")]
    // public static void LoadTutorial() { openScene(Scenes.Tutorial); }
    // [MenuItem("Scenes/Load Stadium")]
    // public static void LoadStadium() { openScene(Scenes.Stadium); }
    // [MenuItem("Scenes/Load MiniGame")]
    // public static void LoadMiniGame() { openScene(Scenes.MiniGame); }
    // [MenuItem("Scenes/Load SponsorHome")]
    // public static void LoadSponsorHome() { openScene(Scenes.SponsorHome); }
    // [MenuItem("Scenes/Load Shop")]
    // public static void LoadShop() { openScene(Scenes.Shop); }
    // [MenuItem("Scenes/Leaderboard")]
    // public static void LoadLeaderboard() { openScene(Scenes.Leaderboard); }
    // [MenuItem("Scenes/Tournament")]
    // public static void LoadTournament() { openScene(Scenes.Tournament); }

    private const string PATH = "Assets/scenes/";
    private const string SCENE_SUFFIX = ".unity";

    private static void openScene(EditorScenes scene)
    {
        if (EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo())
            EditorSceneManager.OpenScene(GetScenePath(scene.Name()), OpenSceneMode.Single);
    }

    private static string GetScenePath(string sceneName)
    {
        return PATH + sceneName + SCENE_SUFFIX;
    }
}
#endif