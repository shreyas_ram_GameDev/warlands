﻿
  
public enum EditorScenes
{
    MainMenu,
    TestScene,
    UccDemoForTesting

}

public static class ScenesExtensions
{
    public static string Name(this EditorScenes scene)
    {
        switch (scene)
        {
            case EditorScenes.MainMenu:
                return "MainMenu";
            case EditorScenes.TestScene:
                return "TestScene";
            case EditorScenes.UccDemoForTesting:
                return "UccDemoForTesting";
            default:
                return "Scene Not Found";
        }
    }
}