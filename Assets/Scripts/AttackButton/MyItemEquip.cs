﻿using Opsive.Shared.Input;
using Opsive.Shared.Input.VirtualControls;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Photon.Pun;

public class MyItemEquip : MonoBehaviourPunCallbacks
{
    public Button[] _attackButton;
    public Sprite[] _buttonSprtie;
    public Sprite _sprite;
    public uint _id;
    public List<string> _inputNames = new List<string>();

    public bool _forloop;


    private void start()
    {
        _forloop = true;
    }


    public void SubscribeToEvents()
    {
        MyPunCallbacks.Instance.ItemPicked += ItemPickedup;

    }

    public void ItemPickedup(uint id)
    {

        if (id == 0) return;
        _id = id;
         ActivateButton(); 
    }

    public void ActivateButton()
    {

        for (int i = 0; i < _attackButton.Length; i++)
        {

            if (_attackButton[i].GetComponent<MyAttackButton>()._buttonIsActive == false)
            {

                _attackButton[i].interactable = true;
                _attackButton[i].GetComponent<Image>().enabled = true;
                _attackButton[i].GetComponent<MyVirtualButton>().enabled = true;
                if (_id == 15)                                                                                   //Knife Item
                {

                    SetButtonSprite(15);
                    _attackButton[i].GetComponent<Image>().sprite = _sprite;
                    MyAttackButton myAttackButton = _attackButton[i].GetComponent<MyAttackButton>();
                    myAttackButton._buttonIsActive = true;
                    myAttackButton._itemSet = 2;
                    myAttackButton._buttonDisableAmount = 3f;
                    myAttackButton.ButtonName();
                    //_attackButton[i].GetComponent<MyAttackButton>()._itemSet = 2;
                    //_attackButton[i].GetComponent<MyAttackButton>()._buttonDisableAmount = 3f;
                    //_attackButton[i].GetComponent<MyAttackButton>().ButtonName();
                    break;

                }
                else if (_id == 20)                                                                               //Heal Item
                {
                    
                    SetButtonSprite(20);
                    _attackButton[i].GetComponent<Image>().sprite = _sprite;
                    MyAttackButton myAttackButton = _attackButton[i].GetComponent<MyAttackButton>();
                    myAttackButton._buttonIsActive = true;
                    myAttackButton._itemSet = 1;
                    myAttackButton._buttonDisableAmount = 15f;
                    myAttackButton.ButtonName();
                    //_attackButton[i].GetComponent<MyAttackButton>()._itemSet = 1;
                    //_attackButton[i].GetComponent<MyAttackButton>()._buttonDisableAmount = 15f;
                    //_attackButton[i].GetComponent<MyAttackButton>().ButtonName();
                    break;

                }
                else if (_id == 19)                                                                                //Fireball Item
                {
                   
                    SetButtonSprite(19);
                    _attackButton[i].GetComponent<Image>().sprite = _sprite;
                    MyAttackButton myAttackButton = _attackButton[i].GetComponent<MyAttackButton>();
                    myAttackButton._buttonIsActive = true;
                    myAttackButton._itemSet = 3;
                    myAttackButton._buttonDisableAmount = 8f;
                    myAttackButton.ButtonName();
                    //_attackButton[i].GetComponent<MyAttackButton>()._itemSet = 3;
                    //_attackButton[i].GetComponent<MyAttackButton>()._buttonDisableAmount = 8f;
                    //_attackButton[i].GetComponent<MyAttackButton>().ButtonName();
                    break;
                }


            }
        }
    }

    public void SetButtonSprite(int id)
    {
        foreach (var sprite in _buttonSprtie)
        {
            int value;
            int.TryParse(sprite.name, out value);
            if (value == id )
            {
                _sprite = sprite;
            }
             
        }
    }
    public void SetItem(int itemSet)
    {
     
        MyPunCallbacks.Instance.Equip(itemSet);
        MyPunCallbacks.Instance.useAbility.StartAbility();
    }
    public void OnDeath()
    {
        
        for (int i = 0; i < _attackButton.Length; i++)
        {
            RemoveInputKey();
            MyAttackButton myAttackButton = _attackButton[i].GetComponent<MyAttackButton>();
            myAttackButton._itemSet = 0;
          //  myAttackButton.usableItem = null;
            myAttackButton._buttonDisableAmount = 0;
            myAttackButton._buttonIsActive = false;
            _attackButton[i].interactable = false;
            _attackButton[i].GetComponent<Image>().enabled = false;
            _attackButton[i].GetComponent<MyVirtualButton>().enabled = false;
        }
    }
    private void RemoveInputKey()
    {
        foreach (var name in _inputNames)
        {
            for (int i = 0; i < _attackButton.Length; i++)
            {
                _attackButton[i].GetComponent<MyVirtualButton>().UnRegisterButton(name);
            }
        }
    }


    public void OnDestroy()
    {
      // MyPunCallbacks.Instance.ItemPicked -= ItemPickedup;
    }
}
