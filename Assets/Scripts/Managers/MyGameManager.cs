﻿using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.SceneManagement;
using Opsive.UltimateCharacterController.Character;
using System;
using UnityEngine.AI;
using Hashtable = ExitGames.Client.Photon.Hashtable;

public class MyGameManager : MonoBehaviourPunCallbacks
{
    public List<GameObject> _blueTeamPlayers = new List<GameObject>();
    public List<GameObject> _redTeamPlayers = new List<GameObject>();
    public static MyGameManager Instance;
    public float TeamCode = 3;
    [SerializeField] public GameObject InstantiatedplayerPrefab;

    [SerializeField] protected GameObject _mainCamera;
    [SerializeField] protected GameObject _spawnCameraBlue;
    [SerializeField] protected GameObject _spawnCameraRed;
    [SerializeField] protected GameObject _dragonRed;
    [SerializeField] protected GameObject _dragonBlue;

    [SerializeField] public int AllPlayerCounts;

    public float _startTimer;
    public float _gameTimer = 600;
    public bool _readytoStart;
    private bool startinggame;
    private bool _startGameTimer;

    public static event Action SpawnEvent;
    public const byte SendTeamListBlue = 1;
    public const byte SendTeamListRed = 2;


    public const byte PlayerInstantiation = 155; // A player has joined the room and been instantiated.


     /// <summary>
    /// Initialize the default values.
    /// </summary>
    void Awake()
    {
        Instance = this;
        GameNetwork.OnSceneUnLoaded += OnSceneChanged;
        UnityEngine.SceneManagement.SceneManager.sceneLoaded += OnSceneLoaded;
        _dragonBlue.SetActive(false);
        _dragonRed.SetActive(false);
       

    }

    private void Start()
    {
       
        _startTimer = 10;
        startinggame = true;
    }

    /// <summary>
    /// Ads player to the Local list as per the team
    /// </summary>
    [PunRPC]
    public void RegisterPlayerBlueTeam(int senderview)
    {  
        var sender = PhotonView.Find(senderview).gameObject;
        Debug.Log(_blueTeamPlayers.Contains(sender));
        if (!_blueTeamPlayers.Contains(sender))
        {
          
            _blueTeamPlayers.Add(sender);
        }
      

    }

    
    public  void UnRegisterPlayerBlueTeam(int senderview)
    {
       
        var sender = PhotonView.Find(senderview).gameObject;
       
    }
    /// <summary>
    /// Ads player to the Local list as per the team
    /// </summary>
    [PunRPC]
    public void RegisterPlayerRedTeam(int senderview)
    {
        Debug.Log(senderview);
        var sender = PhotonView.Find(senderview).gameObject;
        if (!_redTeamPlayers.Contains(sender))
        {
           
            _redTeamPlayers.Add(sender);
        }
       
    }
    public  void UnRegisterPlayerRedTeam(int senderview)
    {
        var sender = PhotonView.Find(senderview).gameObject;
        _redTeamPlayers.Remove(sender);
    }
    //public void UpdatePlayePrefabList()
    //{
    //    object BlueList = _blueTeamPlayers;
    //    object RedList = _redTeamPlayers;
    //    RaiseEventOptions raiseEventOptions = new RaiseEventOptions { Receivers = ReceiverGroup.All };
    //    PhotonNetwork.RaiseEvent(SendTeamListBlue, _blueTeamPlayers, raiseEventOptions, SendOptions.SendReliable);
    //    PhotonNetwork.RaiseEvent(SendTeamListRed, _redTeamPlayers, raiseEventOptions, SendOptions.SendReliable);
    //}

    //public void OnEvent(EventData photonEvent)
    //{
    //    byte eventCode = photonEvent.Code;
    //    if (eventCode == SendTeamListBlue)
    //    {
    //        List<GameObject> data = (List<GameObject>)photonEvent.CustomData;
    //        data = _blueTeamPlayers;
    //    }
    //    else if (eventCode == SendTeamListRed)
    //    {
    //        List<GameObject> data = (List<GameObject>)photonEvent.CustomData;
    //        data = _redTeamPlayers; ;
    //    }
    //}

    void OnSceneLoaded(Scene scene, LoadSceneMode loadingMode)
    {
        if (scene.buildIndex == 1)
        {
            this.CalledOnLevelWasLoaded(2);
        }
    }

    void CalledOnLevelWasLoaded(int level)
    {
        //When this level is loaded
    }
    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        //When  player enters
    }

   
    void Update()
    {

        GameTimer();
        WaitingforAllPlayers();

    }

    /// <summary>
    /// keep counts of all the players and bots in the game and update UI
    /// </summary>
    public void AllCountInc()
    {
        photonView.RPC("PunAllCount", RpcTarget.OthersBuffered);
    }

    [PunRPC]
    public void PunAllCount()
    {

        AllPlayerCounts++;
        UIManager.Instance.PlayerCount();
    }


    /// <summary>
    /// Waiting for all players and bots to join the team 
    /// </summary>
    [PunRPC]
    public void WaitingforAllPlayers()
    {
        if (_readytoStart == true)
        {
            _startTimer -= Time.deltaTime;
           

        }

        if (_startTimer <= 0f)
        {
            if (startinggame == true)
            {
                // UIManager.Instance._tutorialPanel.SetActive(true);
                // UIManager.Instance.TutorialText(1);
                _readytoStart = false;
                _startGameTimer = true;
                // ShrinkingZoneManager.Instance._canSpwnZone = true;
                if (!PhotonNetwork.IsMasterClient) return;
                photonView.RPC("StartSpawning", RpcTarget.AllBuffered);
            }
        }
    }

    /// <summary>
    /// Spawns player and team at the desried location which user selected when on Dragon
    /// </summary>

    [PunRPC]
    public void StartSpawning()
    {
        startinggame = false;
        _mainCamera.SetActive(false);
        if (TeamCode == 1)
        {
            _spawnCameraBlue.SetActive(true);
            _dragonBlue.SetActive(true);


        }
        else if (TeamCode == 2)
        {
            _spawnCameraRed.SetActive(true);
            _dragonRed.SetActive(true);

        }

    }

    /// <summary>
    /// Sets player and bots position according to the team
    /// </summary>
    public void SetPlayerPosition(Vector3 position, int teamcode)
    {
        if (teamcode == 1)
        {
            Vector3 offset = new Vector3(UnityEngine.Random.Range(10, 20), 0, UnityEngine.Random.Range(10, 20));
            _mainCamera.SetActive(true);
            _spawnCameraBlue.SetActive(false);
            _spawnCameraRed.SetActive(false);
            SpawnEvent();
            SetBotsPositon(position, teamcode);
            InstantiatedplayerPrefab.GetComponent<UltimateCharacterLocomotion>().enabled = false;
            InstantiatedplayerPrefab.transform.position = position + offset;
            InstantiatedplayerPrefab.GetComponent<UltimateCharacterLocomotion>().enabled = true;

        }

        else if (teamcode == 2)
        {
            Vector3 offset = new Vector3(UnityEngine.Random.Range(10, 20), 0, UnityEngine.Random.Range(10, 20));
            _mainCamera.SetActive(true);
            _spawnCameraBlue.SetActive(false);
            _spawnCameraRed.SetActive(false);
            SpawnEvent();
            SetBotsPositon(position, teamcode);
            if (MyPlayerManager.Instance.isLeader == true)
            {
                photonView.RPC("RedBotsPosition", RpcTarget.MasterClient, position);
            }
            InstantiatedplayerPrefab.GetComponent<UltimateCharacterLocomotion>().enabled = false;
            InstantiatedplayerPrefab.transform.position = position;
            InstantiatedplayerPrefab.GetComponent<UltimateCharacterLocomotion>().enabled = true;
        }
    }

    /// <summary>
    /// Sets bots position according to the teams
    /// </summary>
    public void SetBotsPositon(Vector3 target, int _teamcode)
    {
       if (PhotonNetwork.IsMasterClient)
        {
            if (_teamcode == 1)
            {

                foreach (var bots in BotManager.Instance._botListBlue)
                {
                    Vector3 offset = new Vector3(UnityEngine.Random.Range(10,20), 0, UnityEngine.Random.Range(10, 20));
                    bots.GetComponent<NavMeshAgent>().enabled = false;
                    bots.transform.position = target + offset;
                    if (bots.transform.position == target + offset)
                    {
                        bots.GetComponent<NavMeshAgent>().enabled = true;
                    }
                   
                    BotManager.Instance.SetBotsPosition(target, _teamcode);
                    if (PhotonNetwork.CurrentRoom.PlayerCount == 1)
                    {
                        photonView.RPC("RedBotsPosition", RpcTarget.MasterClient, BotManager.Instance.RedBotsSpawnPoint.position);
                    }

                }
            }
            
        }
    }
    [PunRPC]
    public void RedBotsPosition(Vector3 _target)
    {

        foreach (var bots in BotManager.Instance._botListRed)
        {
            int _teamcode = 2;

            Vector3 offset = new Vector3(UnityEngine.Random.Range(10, 20), 0, UnityEngine.Random.Range(10, 20));
            bots.GetComponent<NavMeshAgent>().enabled = false;
            bots.transform.position = _target + offset;
            if (bots.transform.position == _target + offset)
            {
                bots.GetComponent<NavMeshAgent>().enabled = true;
            }
            BotManager.Instance.SetBotsPosition(_target, _teamcode);

        }
    }
   


    public void OnSceneChanged()
    {
      
        _blueTeamPlayers.Clear();
        _redTeamPlayers.Clear();

    }
    /// <summary>
    /// Called when players photon properties changes
    /// </summary>
    public override void OnPlayerPropertiesUpdate(Player targetPlayer, Hashtable changedProps)
    {
        
        base.OnPlayerPropertiesUpdate(targetPlayer, changedProps);
        if (changedProps.ContainsKey(PlayerProperty.PLAYER_COLOR))
        {

            UpdateList();
            PlayerColor();
           
        }
        else if (changedProps.ContainsKey(PlayerProperty.PLAYER_READY))
        {
            CheckPlayersReady();
        }

    }
    /// <summary>
    /// Ads player to the Local list as per the team
    /// </summary>
    public void UpdateList()
    {
       // if (PhotonNetwork.CurrentRoom.PlayerCount != PhotonNetwork.CurrentRoom.MaxPlayers) return;
        foreach (Player p in PhotonNetwork.PlayerList)
        {
            object code;
            if (p.CustomProperties.TryGetValue(PlayerProperty.PLAYER_PHOTON_ID, out code))
            {

                var senderid = PhotonView.Find((int)code).gameObject;
                object team;
                if ((p.CustomProperties.TryGetValue(PlayerProperty.PLAYER_COLOR, out team)))
                {
                    var sender = (float)team;

                    if (sender == 1)
                    {
                       
                        _blueTeamPlayers.Add(senderid);
                    }
                    else if (sender == 2)
                    {
                        
                        _redTeamPlayers.Add(senderid);
                    }
                }

               
            }
        }
           
    }

    /// <summary>
    /// Sets Player color according to the teams they are in
    /// </summary>
    public void PlayerColor()
    {
       
        foreach (Player p in PhotonNetwork.PlayerList)
        {
            object playerColor;
            if (p.CustomProperties.TryGetValue(PlayerProperty.PLAYER_COLOR, out playerColor))
            {

                if ((float)playerColor == 1)
                {
                   
                    foreach (var player in _blueTeamPlayers)
                    {
                        
                        player.GetComponent<MyPlayerManager>().TeamCode = 1;
                        player.GetComponentInChildren<SpriteRenderer>().color = Color.blue;
                        player.GetComponentInChildren<MyPlayerUI>().playerTeam.color = Color.blue;
                        
                      
                    }
                }
                else if((float)playerColor == 2)
                {
                    foreach (var player in _redTeamPlayers)
                    {
                        player.GetComponent<MyPlayerManager>().TeamCode = 2;
                        player.GetComponentInChildren<SpriteRenderer>().color = Color.red;
                        player.GetComponentInChildren<MyPlayerUI>().playerTeam.color = Color.red;

                    }
                }
            }
        }
    }
    /// <summary>
    /// Checks if all the player are ready
    /// </summary>
    public bool CheckPlayersReady()
    {
        if (MyGameManager.Instance.AllPlayerCounts != PhotonNetwork.CurrentRoom.MaxPlayers)
            return false;

        foreach (Player p in PhotonNetwork.PlayerList)
        {
            object isPlayerReady;
            if (p.CustomProperties.TryGetValue(PlayerProperty.PLAYER_READY, out isPlayerReady))
            {
                if ((bool)isPlayerReady == false)
                {
                    return _readytoStart = false;
                }
            }
            else
            {

                return _readytoStart = false;
            }
        }
        
        return _readytoStart = true;
    }

    /// <summary>
    /// Game timer starts when everyone is spawned
    /// </summary>
    public void GameTimer()
    { 
        if (_startGameTimer == false) return;
        _gameTimer -= Time.deltaTime;
        if (_gameTimer <= 0)
        {
            GameEnd();
        }
    }

    public void GameEnd()
    {
        throw new NotImplementedException();
    }

    private void OnDestroy()
    {
        GameNetwork.OnSceneUnLoaded -= OnSceneChanged;
    }

   
}




