﻿using Photon.Pun;
using UnityEngine;
using UnityEngine.UI;
using Opsive.UltimateCharacterController.Traits;
using Photon.Realtime;
using Photon.Pun.UtilityScripts;
using System.Collections;
using Opsive.UltimateCharacterController.Items.Actions;
using System;

public class MyPlayerManager : MonoBehaviourPunCallbacks
{

    [SerializeField] public SpriteRenderer TeamIndicator;

   
    public Image _healthbar;
    public float TeamCode;
    public string _playerHud;

    public AbilityControler abilityControler;
    public bool isReady;
    public bool isLeader;
    public bool zoneLeft;

    private Health health;
    public static MyPlayerManager Instance;
    public static GameObject LocalPlayerInstance;

    private MeleeWeapon meleeWeapon;

    public GameObject spawnFX;


    private void Awake()
    {
        if(Instance == null)
        {
            MyPlayerManager.LocalPlayerInstance = this.gameObject;
            MyPlayerManager.Instance = this;
        }

    }
    public void Start()
    {

        health = GetComponent<CharacterHealth>();
        abilityControler = FindObjectOfType<AbilityControler>();
        meleeWeapon = FindObjectOfType<MeleeWeapon>();
        if (photonView.IsMine)
        {
            if (!PhotonNetwork.IsMasterClient)
            {
               // MyGameManager.Instance.AllPlayerCounts++;
            }
            else
            {
                isLeader = true;
            }
            if (PhotonNetwork.CurrentRoom.PlayerCount == 2)
            {
                isLeader = true;
            }
            JoinTeam(PhotonNetwork.LocalPlayer);
            isReady = false;
            if (MyGameManager.Instance.InstantiatedplayerPrefab != null) return;
            MyGameManager.Instance.InstantiatedplayerPrefab = PhotonView.Find(this.photonView.ViewID).gameObject;
            AddtoPlayerList();
        }


        MyPath.OnDragonLandedEvent += HandleDragonLanded;

    }

    private void HandleDragonLanded()
    {
        spawnFX.SetActive(true);
    }

    public void AddtoPlayerList()
    {
        
        if ( TeamCode == 1) 
        {
            MyGameManager.Instance.TeamCode = 1;
            TeamIndicator.color = Color.blue;
            this.gameObject.layer = LayerMask.NameToLayer(TeamManager.TeamLayer.TeamBlue.ToString());
            meleeWeapon.ImpactLayers = LayerMask.GetMask(TeamManager.TeamLayer.TeamRed.ToString()); //enemy layer
            SetInitProp();
            SetInitProp2();
        }
        else if(TeamCode == 2) 
        {
            MyGameManager.Instance.TeamCode = 2;
            TeamIndicator.color = Color.red;
            this.gameObject.layer = LayerMask.NameToLayer(TeamManager.TeamLayer.TeamRed.ToString());
            meleeWeapon.ImpactLayers = LayerMask.GetMask(TeamManager.TeamLayer.TeamBlue.ToString()); //enemy layer

            SetInitProp();
            SetInitProp2();
        }

    }



    /// <summary>
    /// Ads player to the Photon Team Manager
    /// </summary>
    public void JoinTeam(Player newPlayer)
    {

        string blue = "Blue";
        string red = "Red";
        if (PhotonNetwork.IsMasterClient)
        {
            newPlayer.JoinTeam(blue);
            TeamCode = 1;
        }

        if (MyGameManager.Instance.AllPlayerCounts % 2 == 0)
        {
            newPlayer.JoinTeam(red);
            TeamCode = 2;
          
        }
        else if(MyGameManager.Instance.AllPlayerCounts%2 != 0)
        {
            newPlayer.JoinTeam(blue);
            TeamCode = 1;
           
        }
      
    }

    public void SetInitProp()
    {
        if (!photonView.IsMine) return;
        ExitGames.Client.Photon.Hashtable initialProps = new ExitGames.Client.Photon.Hashtable() { { PlayerProperty.PLAYER_COLOR, TeamCode}, {PlayerProperty.PLAYER_PHOTON_ID, this.photonView.ViewID } };
        PhotonNetwork.LocalPlayer.SetCustomProperties(initialProps);
    }
    public void SetInitProp2()
    {
        if (!photonView.IsMine) return;
        ExitGames.Client.Photon.Hashtable initial = new ExitGames.Client.Photon.Hashtable() { { PlayerProperty.PLAYER_PHOTON_ID, this.photonView.ViewID } };
        PhotonNetwork.LocalPlayer.SetCustomProperties(initial);
    }
    public void SetInitProp3()
    {
        if (!photonView.IsMine) return;
        ExitGames.Client.Photon.Hashtable initial = new ExitGames.Client.Photon.Hashtable() { { PlayerProperty.Is_PlayerLeader, isReady } };
        PhotonNetwork.LocalPlayer.SetCustomProperties(initial);
    }


    public void ZoneLeftDamage()
    {
        if (!photonView.IsMine) return;
        if (ShrinkingZoneManager.Instance._canSpwnZone == true) return;
        health.Damage(20f);
        StartCoroutine(Damage());
        
        
    }

    private IEnumerator Damage()
    {
        if (zoneLeft == false) yield break;
        yield return new WaitForSeconds(4);
        ZoneLeftDamage();
      
    }

    private void OnDisable() {
        MyPath.OnDragonLandedEvent -= HandleDragonLanded;
    }

}




