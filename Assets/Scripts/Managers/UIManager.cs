﻿
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;
using TMPro;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class UIManager : MonoBehaviourPunCallbacks
{
    [SerializeField] protected GameObject _baseAttackbutton;
    [SerializeField] protected GameObject _miniMap;
    [SerializeField] protected GameObject _readyButtonContainer;
    [SerializeField] protected GameObject _starUIHolder;
    [SerializeField] public GameObject _tutorialPanel;

    [SerializeField] protected Image _whiteTransition;
    [SerializeField] protected Image _starUI;


    [SerializeField] protected Text _startCount = null;
    [SerializeField] protected Text _waitngForPlayers = null;
    [SerializeField] protected Text _playerCount = null;
    [SerializeField] protected TextMeshProUGUI _gamerTimerText = null;
    [SerializeField] protected TextMeshProUGUI _starCount = null;
    [SerializeField] protected TextMeshProUGUI _tutorialText = null;

    [SerializeField] protected Button _playerReadyButton;
    public Button _addbots;

    [SerializeField] public int _currentPlayer;
   
    public static UIManager Instance;
    public MyGameManager MasterManager;

    public override  void OnEnable()
    {
        MyGameManager.SpawnEvent += PlayerSpawned;
    }
    public override void OnDisable()
    {
        MyGameManager.SpawnEvent -= PlayerSpawned;
    }
    private void Awake()
    {
        Instance = this;
     
    }
   

    void Start()
    {
        
            PlayerCount();
        
        _starUIHolder.SetActive(false);
        _startCount.enabled = false;
        // _tutorialPanel.SetActive(false);
         _baseAttackbutton.SetActive(false);
      //   _baseAttackbutton2.SetActive(false);
        _miniMap.SetActive(false);
        int a = 2;
        MasterManager = PhotonView.Find(a).GetComponent<MyGameManager>();

        _playerReadyButton.onClick.AddListener(() =>
        {
            _playerReadyButton.interactable = false;
            MyPlayerManager.Instance.isReady = !MyPlayerManager.Instance.isReady;
            SetPlayerReady(MyPlayerManager.Instance.isReady);

            Hashtable props = new Hashtable() { { PlayerProperty.PLAYER_READY, MyPlayerManager.Instance.isReady } };
            PhotonNetwork.LocalPlayer.SetCustomProperties(props);

        });



    }

    void Update()
    {
        PhotonUISync();
        GameTimerDisplay(MyGameManager.Instance._gameTimer);
    }
  
    public void PhotonUISync()
    {
        //_playerCount.text = PhotonNetwork.CurrentRoom.PlayerCount.ToString() + "/" + PhotonNetwork.CurrentRoom.MaxPlayers;
        if (MasterManager._readytoStart == true)
        {
          
            _startCount.enabled = true;
            _startCount.text = "Starting in " + MasterManager._startTimer.ToString("f0");
        }

        if (MasterManager._startTimer <= 0)
        {
          
          
            _playerReadyButton.enabled = false;
            _readyButtonContainer.SetActive(false);
            _startCount.enabled = false;
            _playerCount.enabled = false;
            _waitngForPlayers.enabled = false;
            _starUIHolder.SetActive(true);
        }
    }
   
    public void StartCountSync()
    {
        int a = 1;
        var timer = PhotonView.Find(a).GetComponent<MyGameManager>()._startTimer;
        _startCount.enabled = true;
        _startCount.text = "Starting in" + timer.ToString("f0");
    }
    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
       
        PlayerCount();

    }

    public void PlayerCount()
    {
       // _currentPlayer++;
        _playerCount.text = MyGameManager.Instance.AllPlayerCounts + "/" + PhotonNetwork.CurrentRoom.MaxPlayers;
        
    }

    public void PlayerSpawned()
    {
        // _tutorialPanel.SetActive(false);
        _whiteTransition.GetComponent<Animation>().Play();
        _baseAttackbutton.SetActive(true);
        _miniMap.SetActive(true);
     
    }
   
    public void SetPlayerReady(bool isReady)
    {

        _playerReadyButton.GetComponentInChildren<Text>().text = isReady ? "Ready!" : "Ready?";
        _playerReadyButton.GetComponent<Image>().color = Color.green;
    }

    public void GameTimerDisplay(float _timer)
    {
       
        float minutes = Mathf.FloorToInt(_timer / 60);
        float seconds = Mathf.FloorToInt(_timer % 60);

        _gamerTimerText.text = string.Format("{0:00}:{1:00}", minutes, seconds);
    }

    public void StarCountDisplay(float teamcode, int count)
    {
        if (teamcode == 1)
        {
            _starCount.text = count + " / 5";
        }
        else
        {
            _starCount.text = count + " / 5";
        }
    }

    public void TutorialText(int a)
    {
        if (a == 1)
        {
            _tutorialText.text = "Touch anywhere on the Ground to land the Dragon";
        }
        else if(a == 2)
        {
            
            _tutorialText.text = "Collect all the stars before the timer reaches 0";
        }
    }
}
