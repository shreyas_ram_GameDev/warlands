﻿using UnityEngine;
using DG.Tweening;
using Photon.Pun;
using TMPro;
using UnityEngine.UI;

public class ShrinkingZoneManager : MonoBehaviourPunCallbacks
{
    public GameObject ShrinkZoneGo;
    public bool _canSpwnZone;
    [SerializeField] private float _timeLeftToSpawn = 120;
    [SerializeField] private float _zoneShrinkTime = 0;
    public Transform[] spwnLocations;

    private Vector3 initialScale;
    private ShrinkingZone shrinkingZone;

    [Header("UI")]
    public TextMeshProUGUI _shrinkText;
    public Image _miniMapBorder;

    public static ShrinkingZoneManager Instance;
    public bool _start;
    public float _startTIme = 15f;
    private Vector3 _miniMapInitialScale;

    private Color Redcolor;
    private Color Bluecolor;

    private void Start()
    {
      
        if (Instance == null)
        {
            Instance = this;
        }
        _canSpwnZone = false;
        shrinkingZone = ShrinkZoneGo.GetComponent<ShrinkingZone>();
        initialScale = ShrinkZoneGo.transform.localScale;
        ShrinkZoneGo.SetActive(false);
        _miniMapInitialScale = _miniMapBorder.transform.localScale;
        ColorUtility.TryParseHtmlString("#CF3C26", out Redcolor);
        ColorUtility.TryParseHtmlString("#3655E2", out Bluecolor);
    }

    private void Update()
    {
        if (PhotonNetwork.IsMasterClient)
        {
            if (_canSpwnZone == true)
            {
                _zoneShrinkTime += Time.deltaTime;
                if (_zoneShrinkTime >= _timeLeftToSpawn)
                {
                    _zoneShrinkTime = 0;
                    _canSpwnZone = false;
                    int random = UnityEngine.Random.Range(0, spwnLocations.Length);
                    photonView.RPC("SpawnZone", RpcTarget.All, random);
                    photonView.RPC("UIUpdate", RpcTarget.All);
                    // photonView.RPC("UpdateBotLocation", RpcTarget.All, random);

                }
            }
        }

        if (_start == true)
        {
            _startTIme -= Time.deltaTime;
            _miniMapBorder.transform.localScale = new Vector3(Mathf.PingPong(Time.time, (_miniMapInitialScale.x + 0.5f) - _miniMapInitialScale.x) + _miniMapInitialScale.x, Mathf.PingPong(Time.time, (_miniMapInitialScale.x + 0.5f) - _miniMapInitialScale.x) + _miniMapInitialScale.x, _miniMapInitialScale.z);
            if (_startTIme < 0)
            {
                _start = false;
                _miniMapBorder.transform.localScale = _miniMapInitialScale;
                _miniMapBorder.color = Bluecolor;
                _shrinkText.enabled = false;

            }
        }

    }

    [PunRPC]
    public void SpawnZone(int random)
    {      
        ShrinkZoneGo.SetActive(true);
        shrinkingZone.OnStart();
        ShrinkZoneGo.transform.position = spwnLocations[random].position;
        ShrinkZoneGo.transform.localScale = initialScale;
    }

    [PunRPC]
    public void UIUpdate()
    {
        _start = true;
        _shrinkText.enabled = true;
        _miniMapBorder.color = Redcolor;
    }

    [PunRPC]
    public void UpdateBotLocation(int random)
    {
        Vector3 target = spwnLocations[random].position;
        foreach (var bot in BotManager.Instance._botListBlue)
        {

            BotMovement botMovement = bot.GetComponent<BotMovement>();
            botMovement.m_center = target;
            botMovement.DetermineDestination();
        }
        foreach (var bot in BotManager.Instance._botListRed)
        {
            BotMovement botMovement = bot.GetComponent<BotMovement>();
            botMovement.m_center = target;
            botMovement.DetermineDestination();
        }
    }
}
