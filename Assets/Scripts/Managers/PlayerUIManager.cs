﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUIManager : MonoBehaviour
{
    public Image _teamIndicator;
    public Text _teamText;
    string blue = "Blue";
    string red = "Red";

    //public Image _healthBar;
   // public static PlayerUIManager instance;
    // Start is called before the first frame update
    void Start()
    {
       
        _teamIndicator.enabled = false;
        StartCoroutine(TeamSelection());
    }

    // Update is called once per frame
   
    public IEnumerator TeamSelection()
    {
        yield return new WaitForSeconds(0.5f);

       
        if (MyPlayerManager.Instance .TeamCode == 1)
        {
            _teamIndicator.enabled = true;
            _teamIndicator.color = Color.blue;
            _teamText.text = "TeamRed";


        }
        else if (MyPlayerManager.Instance.TeamCode == 2)
        {
            _teamIndicator.enabled = true;
            _teamIndicator.color = Color.red;
            _teamText.text = "TeamBlue";
        }

       
    }

    public void Healthbar(float amount)
    {
        //_healthBar.fillAmount -= amount/100;
    }

   
}
