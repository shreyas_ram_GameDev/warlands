﻿
using UnityEngine;
using DG.Tweening;
using Opsive.UltimateCharacterController.Utility;
using Opsive.UltimateCharacterController.Game;
using Opsive.UltimateCharacterController.Traits;

public class ShrinkingZone : MonoBehaviour
{
    [SerializeField] private float _scaleToSize = 200f;
    [SerializeField] private float _scaleDuration = 180f;

    [SerializeField] private float _damage;
    public Health health;

    public void OnStart()
    {
        
        transform.DOScale(_scaleToSize, _scaleDuration);
    }

    private void Update()
    {
        if (transform.localScale == new Vector3(_scaleToSize, _scaleToSize, _scaleToSize))
        {

            ShrinkingZoneManager.Instance._canSpwnZone = true;
            gameObject.SetActive(false);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (MathUtility.InLayerMask(other.gameObject.layer, 1 << LayerManager.Character))
        {
            if (other.transform.root.GetComponent<MyPlayerManager>() != null)
            {
                other.transform.root.GetComponent<MyPlayerManager>().zoneLeft = false;
            }
            else
            {
                other.transform.root.GetComponent<MyBotManager>().zoneLeft = false;
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (MathUtility.InLayerMask(other.gameObject.layer, 1 << LayerManager.Character))
        {
            if (other.transform.root.GetComponent<MyPlayerManager>() != null)
            {
                MyPlayerManager myPlayer = other.transform.root.GetComponent<MyPlayerManager>();
                myPlayer.zoneLeft = true;
                myPlayer.ZoneLeftDamage();
            }
            else
            {
                MyBotManager myBotManager = other.transform.root.GetComponent<MyBotManager>();
                myBotManager.zoneLeft = true;
                myBotManager.ZoneLeftDamage();
            }


        }
    }




}
