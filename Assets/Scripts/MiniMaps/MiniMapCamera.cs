﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniMapCamera : MonoBehaviour
{
    public Transform _player;
    void Start()
    {
        if (MyGameManager.Instance.InstantiatedplayerPrefab != null)
        {
            _player = MyGameManager.Instance.InstantiatedplayerPrefab.transform;
        }
    }

   
    void LateUpdate()
    {
        if (_player != null)
        {
            Vector3 nePos = _player.position;
            nePos.y = transform.position.y;
            transform.position = nePos;
        }
        else
        {
            if (MyGameManager.Instance.InstantiatedplayerPrefab != null)
            {
                _player = MyGameManager.Instance.InstantiatedplayerPrefab.transform;
            }
        }
    }
    
}
