﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniMapHandler : MonoBehaviour
{
    public GameObject[] spriteRenderers;

    Color blue;
    Color red;

    public static MiniMapHandler Instance;

    private void Start()
    {
        if(Instance == null)
        {
            Instance = this;
        }

        ColorUtility.TryParseHtmlString("#2DABDD", out blue);
        ColorUtility.TryParseHtmlString("#DD352D", out red);
        blue.a = 0.6f;
    }
    public void UnlockArea(float teamcode, string landname)
    {
        foreach (var sprite in spriteRenderers)
        {

            if (sprite.name == landname)
            {
                
                sprite.SetActive(true);
                if (teamcode == 1)
                {
                    
                    sprite.GetComponent<SpriteRenderer>().color = blue;
                   
                    
                }
                else
                {
                   
                    sprite.GetComponent<SpriteRenderer>().color = red;
                   
                }
                return;
            }
        }
    }
}
