﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimation : MonoBehaviour
{
    public Animator _playerAnim;
    float AnimSmoothTime = 0.1f;

    PlayerController _player;
    // Start is called before the first frame update
    void Start()
    {
        _player = FindObjectOfType<PlayerController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (_player.EnableJoystick)                                                       //Speed for Joystick Control
        {
            float speed = _player.rb.velocity.magnitude / _player.speed;                  //Calculating speed of animation with the speed of Character Moving
            _playerAnim.SetFloat("Speed", speed, AnimSmoothTime, Time.deltaTime);
        }
        else
        {
            float speed = _player._navAgent.velocity.magnitude / _player._navAgent.speed;   //Speed for Touch Control
            _playerAnim.SetFloat("Speed", speed, AnimSmoothTime, Time.deltaTime);
        }
       
    }
}
