﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowOld : MonoBehaviour
{
    PlayerController  _player;
    private Vector3 offset;
    [Range(0.01f, 1.00f)]
    public float CameraSmoothness = 0;
    // Start is called before the first frame update
    void Start()
    {
        _player = FindObjectOfType<PlayerController>();
        offset = transform.position - _player.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 newPos = _player.transform.position + offset;              //Taking the position of player and adding offset 
        transform.position = Vector3.Slerp(transform.position, newPos, CameraSmoothness);
    }
}
