﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerUI : MonoBehaviour
{
    [SerializeField] public GameObject JoystickPrefab;

    PlayerController _player;

    // Start is called before the first frame update
    void Start()
    {
        _player = FindObjectOfType<PlayerController>();
        if (_player.EnableJoystick)
            JoystickPrefab.SetActive(true);
        else
            JoystickPrefab.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ChooseControlUI()
    {
        _player.EnableJoystick = true;
    }
}
