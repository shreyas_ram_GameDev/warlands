﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


public class PlayerController : MonoBehaviour
{
    [Header("Joystick")]
    private VariableJoystick variableJoystick;
    Vector3 _moveDirection;
  

    [Header("TouchControl")]
    [SerializeField] public float rotateSpeed = 0.75f;
    [HideInInspector]
    public NavMeshAgent _navAgent;   
    private float rotateVelocity;
    Vector2 _touchOrigin;

    [Header("Control")]
    public bool EnableJoystick = true;

    [Header("General")]
    [HideInInspector]
    public Rigidbody rb;
    [SerializeField ]public float speed = 5;


    // Start is called before the first frame update
    void Start()
    {
      
        variableJoystick = FindObjectOfType<VariableJoystick>();
        rb = gameObject.GetComponent<Rigidbody>();
        _navAgent = gameObject.GetComponent<NavMeshAgent>();
    }
   
    // Update is called once per frame
    void Update()
    {
        if (EnableJoystick)
        {
            JoystickMovement();
        }
        else
        {
            if (Application.isEditor)
                EditorInput();
            else
                MobileInput();
        }
    }

   

    void MobileInput()
    {
        switch (Input.touchCount)                     //Using Switch to check number of touchCount to give accuracy when using zoom in etc
        {
            case 1:
                TouchMovement();
                break;
            case 2:
                //Do stuff
                break;
        }
    }
    
    protected void  JoystickMovement() //For Joystick Control 
    {
        _moveDirection = new Vector3(variableJoystick.Horizontal * speed + Input.GetAxis("Horizontal") * speed, 0, variableJoystick.Vertical * speed + Input.GetAxis("Vertical") * speed);
        rb.velocity = _moveDirection;
        if(_moveDirection != Vector3.zero)                                                           // Checking if Vector3 is not zero so that LookRotation dosent becomes zero
            transform.rotation = Quaternion.LookRotation(_moveDirection.normalized);

    }

    protected void TouchMovement() // For Tap and Move Control Mobile
    {
        if (Input.touchCount > 0 && Input.touches[0].phase == TouchPhase.Began)
        {
            _touchOrigin = Input.touches[0].position;
            RaycastHit hit;

            if (Physics.Raycast(Camera.main.ScreenPointToRay(_touchOrigin), out hit, Mathf.Infinity))
            {
                _navAgent.SetDestination(hit.point);
            }
            Quaternion rotationToLookAt = Quaternion.LookRotation(hit.point - transform.position);
            float rotationY = Mathf.SmoothDampAngle(transform.eulerAngles.y, rotationToLookAt.eulerAngles.y, ref rotateVelocity, rotateSpeed * (Time.deltaTime * 5));

            transform.eulerAngles = new Vector3(0, rotationY, 0);
        }
    }
    protected void EditorInput()  // For Tap and Move Control Editor
    {
       
        if (Input.GetMouseButtonDown(0))
        {
           
            RaycastHit hit;

            // Raycast and hit to Move Player to Hit point
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, Mathf.Infinity))
            {
                _navAgent.SetDestination(hit.point);

            }

            //Rotate Player 
            Quaternion rotationToLookAt = Quaternion.LookRotation(hit.point - transform.position);
            float rotationY = Mathf.SmoothDampAngle(transform.eulerAngles.y, rotationToLookAt.eulerAngles.y, ref rotateVelocity, rotateSpeed *(Time.deltaTime * 5));

            transform.eulerAngles = new Vector3(0, rotationY, 0);

        }
    }



}
