using Opsive.UltimateCharacterController.Traits;
using UnityEngine;

public class HealingSpot : MonoBehaviour
{
    CharacterHealth health;
    public ParticleSystem healingAuraFx;
    private bool canHeal;

    private void OnTriggerEnter(Collider other) 
    {
        Debug.Log("triggered player");
        Debug.Log(other.gameObject.transform.root);
        if (other.gameObject.transform.root.GetComponent<CharacterHealth>() != null && other.gameObject.layer == LayerMask.NameToLayer("Character"))
        {
            canHeal = true;
            healingAuraFx.gameObject.SetActive(true);
            health = other.gameObject.transform.root.GetComponent<CharacterHealth>();
            
        }
    }
    private void OnTriggerStay(Collider other) 
    {
        if(canHeal)
        {
            InvokeRepeating("HealPlayer", 0.1f, 0.5f);
            LoopParticleEffect(true);
        }
    }

    private void OnTriggerExit(Collider other) 
    {
        canHeal = false;
        CancelInvoke("HealPlayer");
        LoopParticleEffect(false);
    }

    void HealPlayer()
    {
        health.Heal(10);
    }

    void LoopParticleEffect(bool isLoop)
    {
        var main = healingAuraFx.main;
        main.loop = isLoop;
    }    
}
