using UnityEngine;
using System.Collections.Generic;

public class InterfaceUtility : MonoBehaviour {
    
    public static void GetInterfaces<T>(out List<T> resultList, GameObject objectToSearch) where T: class {
            MonoBehaviour[] list = objectToSearch.GetComponents<MonoBehaviour>();
            resultList = new List<T>();
            foreach(MonoBehaviour mb in list){
                if(mb is T){
                    //found one
                    resultList.Add((T)((System.Object)mb));
                }
            }
        }
}


