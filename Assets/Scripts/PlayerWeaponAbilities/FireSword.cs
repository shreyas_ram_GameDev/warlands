using UnityEngine;
using DG.Tweening;
using Opsive.UltimateCharacterController.Items.Actions;

public class FireSword : WeaponAbility
{
    MeleeWeapon meleeWeapon;
    private void Awake() {
        WeaponAbilityHandler.ActivateFireAbilityEvent += HandleWeaponAbility;
    }

    private void HandleWeaponAbility()
    {
        ActivateWeaponAbility();
    }

    private void Start() 
    {
        meleeWeapon = this.GetComponent<MeleeWeapon>();
    }

    
    public override void ActivateWeaponAbility()
    {
        weaponAbilityPrefab.SetActive(true);
        weaponAbilityPrefab.transform.DOScaleX(1, 0.3f);
        meleeWeapon.DamageAmount += 30;
        meleeWeapon.Trail.GetComponent<Renderer>().sharedMaterial.SetColor("_EmissionColor", Color.yellow);

        Invoke("DeactivateAbility", 10); //make a global cooldown time variable for both ui condown and event
    }

    public void DeactivateAbility()
    {
        weaponAbilityPrefab.transform.DOScaleX(1, 0.3f).OnComplete(() => weaponAbilityPrefab.SetActive(false));
        meleeWeapon.DamageAmount -= 30;
    }
}