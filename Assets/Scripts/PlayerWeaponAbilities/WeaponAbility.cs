using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
public abstract class WeaponAbility : MonoBehaviourPunCallbacks 
{
    [SerializeField] protected GameObject weaponAbilityPrefab;
    protected float damageAmount;


    public abstract void ActivateWeaponAbility();
}