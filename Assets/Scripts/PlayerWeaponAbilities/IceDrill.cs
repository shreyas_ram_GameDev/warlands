using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class IceDrill : WeaponAbility
{
    private Transform player;
    GameObject icePp;
    private void Awake() {
        WeaponAbilityHandler.ActivateIceAbilityEvent += HandleWeaponAbility;
    }

    private void Start() {
        player = transform.root;
        Debug.Log("icedrill go" + this.gameObject.name);

    }
    private void HandleWeaponAbility()
    {
        ActivateWeaponAbility();
        
    }

    public override void ActivateWeaponAbility()
    {
        // StartCoroutine(ActivateIceDrill());
        icePp = Instantiate(weaponAbilityPrefab, player.transform.position, Quaternion.Euler(-90, 0, 0));
        
        StartCoroutine(CheckRadius());
    }

    IEnumerator CheckRadius()
    {
        Debug.Log("Get iced enemy");
        Collider[] colliders = Physics.OverlapSphere(transform.position, 10);

        foreach (Collider nearby in colliders)
        {
            
            Rigidbody rb = nearby.GetComponent<Rigidbody>();
            if (rb != null)
            {
                List<IFreezable> interfaceList;
                InterfaceUtility.GetInterfaces<IFreezable>(out interfaceList, nearby.gameObject);
                Debug.Log(interfaceList.Count);
                foreach (IFreezable freezable in interfaceList)
                {
                    
                    freezable.FreezeCo();

                }
            }
        }
        yield return new WaitForSeconds(0.8f);
        var simulationSpeed = icePp.GetComponent<ParticleSystem>().main;
        simulationSpeed.simulationSpeed = 0;
        yield return new WaitForSeconds(1);
        simulationSpeed.simulationSpeed = 1;
    }
    private void OnDrawGizmosSelected() 
    {
        Gizmos.color = Color.red;
        //Use the same vars you use to draw your Overlap SPhere to draw your Wire Sphere.
        Gizmos.DrawWireSphere (transform.position , 20);
    }
}
