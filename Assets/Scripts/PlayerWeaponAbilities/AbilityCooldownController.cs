using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;
public class AbilityCooldownController : MonoBehaviour
{
    private float cooldownTime = 10;
    private TextMeshProUGUI cooldownText;
    private Image cooldownImage;
   public void InvokeCoolDown(Button _abilityBtn)
    {
        StartCoroutine(CoolDownTime(_abilityBtn));

        cooldownText = _abilityBtn.gameObject.GetComponentInChildren<TextMeshProUGUI>();
        cooldownImage = _abilityBtn.gameObject.GetComponent<Image>();
        cooldownImage.DOFillAmount(0, cooldownTime);
    }
    IEnumerator CoolDownTime(Button _abilityBtn)
    {
        _abilityBtn.interactable = false;

        float t = cooldownTime;
        while(t > 0)
        {
            t--;
            yield return new WaitForSeconds(1);
            cooldownText.text = t.ToString();
        }
        // yield return new WaitForSeconds(cooldownTime);

        cooldownImage.fillAmount = 1;
        cooldownText.text = "";
        _abilityBtn.interactable = true;
    }
}
