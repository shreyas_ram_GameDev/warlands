using System;
using System.Collections;
using UnityEngine;

public class WeaponAbilityHandler : MonoBehaviour 
{
    public static Action ActivateFireAbilityEvent = delegate { };
    public static Action ActivateIceAbilityEvent = delegate { };

    public void FireAbility()
    {
        // MyAbility myAbility = FireAbility;
        ActivateFireAbilityEvent?.Invoke();
    }
    public void IceAbility()
    {
        // MyAbility myAbility = IceAbility;
        ActivateIceAbilityEvent?.Invoke();
    }
}