using Opsive.UltimateCharacterController.Items.Actions;
using UnityEngine;

public class ComboManager : MonoBehaviour
{
    public int hitCount;
    public GameObject comboFx;
    public MeleeWeapon mw;
    float comboDamageBonus = 10;
    float defaultDamage;

    private void Start(){
        defaultDamage = mw.DamageAmount;
    }
    public void GetComboInfo()
    {
        Debug.Log("Booom " + mw.UseAnimatorAudioStateSet.GetItemSubstateIndex());
        // hitCount++;
        
        if(mw.UseAnimatorAudioStateSet.GetItemSubstateIndex() == 10)
        {
            // comboFx.transform.SetParent(this.transform, true);
            // comboFx.SetActive(true);
            mw.DamageAmount += comboDamageBonus;
            //knockback effect
            KnockBack();
        }
        
    }
    private void OnDrawGizmosSelected() {
        Gizmos.color = Color.red;
        //Use the same vars you use to draw your Overlap SPhere to draw your Wire Sphere.
        Gizmos.DrawWireSphere (transform.position , knockbackRadius);
 }

    #region KnockBack ComboEffect 
    public float knockbackRadius = 100;
    private float knockbackForce = 10000;
    void KnockBack()
    {
        Instantiate(comboFx,transform.position,Quaternion.Euler(-90, 0, 0 ));
        Collider[] colliders = Physics.OverlapSphere(transform.position, knockbackRadius);

        foreach (Collider nearby in colliders)
        {
            
            Rigidbody rb = nearby.GetComponent<Rigidbody>();
            if(rb != null && nearby.gameObject.layer == LayerMask.NameToLayer("Enemy"))
            {
                // Debug.Log(nearby.name);
                rb.AddExplosionForce(knockbackForce, transform.position, knockbackRadius);
            }
        }


        // mw.DamageAmount = defaultDamage;
    }

    #endregion
}
