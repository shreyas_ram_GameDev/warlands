using System.Collections;
using System.Collections.Generic;
using Opsive.UltimateCharacterController.Character.Abilities;
using Opsive.UltimateCharacterController.Character.Abilities.Items;
using Opsive.UltimateCharacterController.Items.Actions;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using Opsive.Shared.Events;

public class ShootableWeaponManager : MonoBehaviour
{
    ShootableWeapon shootableWeaponProperties;
    public Image rangeUI;
    public Image directionUI;
    float fireChargeTime;
    private void Awake() 
    {
    }
    void Start()
    {
        shootableWeaponProperties = GetComponent<ShootableWeapon>();
    }

    private void Update() {
        if(MyPunCallbacks.Instance.useAbility.IsActive)
        {
            // fireChargeTime += Time.deltaTime;
            rangeUI.transform.localScale = Vector3.Lerp(rangeUI.transform.localScale, Vector3.one * 8, 0.5f);
            directionUI.transform.localScale = Vector3.Lerp(directionUI.transform.localScale,new Vector3(directionUI.transform.localScale.x,8,1), 0.5f);
        }
        else
        {
            fireChargeTime = 0;
            // rangeUI.transform.localScale = Vector3.zero;
            rangeUI.transform.localScale = Vector3.Lerp(Vector3.one * 8,Vector3.zero, 0.5f);
            directionUI.transform.localScale = Vector3.Lerp(directionUI.transform.localScale,new Vector3(directionUI.transform.localScale.x,0,1), 0.5f);

        }
    }    
}
