﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarManager : MonoBehaviour
{
    public int _blueTeamStarCount = 0;
    public int _redTeamStarCount = 0;

    
    public Animation starUianim;
    public static StarManager Instance;

    private void Start()
    {
        if (Instance == null)
        {
            Instance = this;

        }

    }

    public void StarCount(int a, float teamcode)
    {

        if (teamcode == 1)
        {
            starUianim.Play();
            _blueTeamStarCount += a;
            UIManager.Instance.StarCountDisplay(teamcode, _blueTeamStarCount);
        }
        else
        {
            starUianim.Play();
            _redTeamStarCount += a;
            UIManager.Instance.StarCountDisplay(teamcode, _redTeamStarCount);
        }
    }

    public void StarCountOnDeath(int a, float teamcode)
    {
        if (teamcode == 1)
        {
            
            _blueTeamStarCount -= a;

            UIManager.Instance.StarCountDisplay(teamcode, _blueTeamStarCount);
        }
        else
        {
            _redTeamStarCount -= a;
            UIManager.Instance.StarCountDisplay(teamcode, _redTeamStarCount);
        }
    }
}


