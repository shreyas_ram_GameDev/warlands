﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

public class MyStars : MonoBehaviourPunCallbacks
{

    public int _myStarCount;

    public void StarCount()
    {
        if (!photonView.IsMine) return;
        _myStarCount += 1;
    }

    public void OnDeath()
    {
        _myStarCount = 0;
    }
}
