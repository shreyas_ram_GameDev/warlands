﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Realtime;

public class MyTeamSpawner : Opsive.UltimateCharacterController.AddOns.Multiplayer.PhotonPun.Game.SpawnManagerBase
{
    [Tooltip("A reference to the character that PUN should spawn. This character must be setup using the PUN Multiplayer Manager.")]
    [SerializeField] protected GameObject m_Character;

    public GameObject Character { get { return m_Character; } set { m_Character = value; } }
    protected override GameObject GetCharacterPrefab(Player newPlayer)
    {
        return m_Character;
    }

    // Start is called before the first frame update
}
