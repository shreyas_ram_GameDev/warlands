﻿
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

public class HealthFlash : MonoBehaviourPunCallbacks
{
    private Image _healthFlash;

    Color _flashColor;
    void Start()
    {
        _healthFlash = GetComponent<Image>();
        _flashColor = _healthFlash.color;
    }

    public void DamageUI(float a, float total)
    {

        float amt = total / a - 1;
        _flashColor.a = amt;
        _healthFlash.color = _flashColor;
        Invoke("FadeOutFlash", 5);
    }

    public void FadeOutFlash()
    {
        _healthFlash.enabled = false;
    }
}
