﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PowerUpButton : MonoBehaviour
{
    public Image _buttonImage;
    bool _start;
    float _startTIme = 3;
    private Vector3 _initialpos;
    void Start()
    {
        _buttonImage = GetComponent<Image>();
        _start = true;
        _initialpos = transform.localScale;
    }

   
    void Update()
    {
        if (_start == true)
        {
            _startTIme -= Time.deltaTime;
            gameObject.transform.localScale = new Vector3(Mathf.PingPong(Time.time, 2), Mathf.PingPong(Time.time, 2), Mathf.PingPong(Time.time, 2));
            if (_startTIme < 0)
            {
                transform.localScale = _initialpos;
                _start = false;
            }
        }
    }
}
