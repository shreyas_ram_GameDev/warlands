﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BilBoardView : MonoBehaviour
{
    public Transform MainCam;
    void Start()
    {
        MainCam = GameObject.FindGameObjectWithTag("MainCamera").transform;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (MainCam == null) return;

        transform.LookAt(transform.position + MainCam.forward);
    }
}
