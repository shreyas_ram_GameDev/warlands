﻿
using Photon.Pun;
using UnityEngine;
using Opsive.UltimateCharacterController.Character.Abilities.Items;
using Opsive.UltimateCharacterController.Character.Abilities;
using Opsive.UltimateCharacterController.Character;
using Opsive.UltimateCharacterController.Inventory;
using Opsive.Shared.Inventory;
using Opsive.UltimateCharacterController.Traits;
using System;
using System.Collections.Generic;
using UnityEngine.Events;

public class MyPunCallbacks : MonoBehaviourPunCallbacks
{
    [SerializeField]public Drop dropAbility;
    private Die dieAbility;
    [SerializeField]public Use useAbility;
    private DamageVisualization damage;
    [SerializeField] protected UltimateCharacterLocomotion characterLocomotion;
    public Health _playerHealth;

    [Tooltip("The ItemType that the character should drop.")]
    [SerializeField] protected ItemType m_ItemTypeStar;
    [SerializeField] public ItemSetManager m_ItemSet;
    [SerializeField] public String StarPickup;

    public MyItemEquip itemEquip;
    public MyStars myStars;
    public MyPlayerUI playerUI;
  

    public List<ItemType> itemTypes = new List<ItemType>();
    public uint _id;

    public bool Died;

    public UnityEvent _playerSpawned;
    public event OnItemPickedupDelegate ItemPicked;
    public delegate void OnItemPickedupDelegate(uint id);
    public uint id
    {
        get
        {
            return _id;
        }
        set
        {
            if (_id == value) return;
            _id = value;
            if (ItemPicked != null)
                ItemPicked(_id);
        }
    }

    private EquipUnequip equipUnequip;
    public static MyPunCallbacks Instance;

  
    public void Start()
    {
        if (Instance == null)
        {
            Instance = this;
        }
      
        itemEquip = FindObjectOfType<MyItemEquip>();
        dropAbility = characterLocomotion.GetAbility<Drop>();
        dieAbility = characterLocomotion.GetAbility<Die>();
        useAbility = characterLocomotion.GetAbility<Use>();
        _playerHealth = characterLocomotion.GetComponent<Health>();
        damage = characterLocomotion.GetAbility<DamageVisualization>();
        m_ItemSet = characterLocomotion.GetComponent<ItemSetManager>();
        equipUnequip = characterLocomotion.GetAbility<EquipUnequip>();
        myStars = GetComponent<MyStars>();
        
        Opsive.Shared.Events.EventHandler.RegisterEvent<Vector3, Vector3, GameObject>(gameObject, "OnDeath", OnDeath);
        Opsive.Shared.Events.EventHandler.RegisterEvent<IItemIdentifier, int, bool, bool>(gameObject, "OnInventoryPickupItemIdentifier", OnItemPickUp);
        if (photonView.IsMine)
        {
            _playerSpawned.AddListener(itemEquip.SubscribeToEvents);
            _playerSpawned.Invoke();
        }
    }

    /// <summary>
    /// Called when an item(Weapons etc) are picked up
    /// </summary>
    public void OnItemPickUp(IItemIdentifier arg1, int arg2, bool arg3, bool arg4)
    {
        if (!photonView.IsMine) return;
        if (Died == true) return;
        ItemType itemType = arg1.GetItemDefinition() as ItemType;
        if (itemType == m_ItemTypeStar)
        {

            StarCount();
            return;
        }
       
        itemTypes.Add(itemType);
        id = itemType.ID;
       

    }

    /// <summary>
    /// Equips the weapon which is assigned to the button which user pressed
    /// </summary>
    public void Equip(int itemSet)
    {
        equipUnequip.StartEquipUnequip(itemSet, false, true);
       
    }
    public void OnDamage()
    {
        characterLocomotion.TryStartAbility(damage);
        if (playerUI == null)
        {
            playerUI =  GetComponentInChildren<MyPlayerUI>();
            playerUI.Health();
        }
        else
            playerUI.Health();

        
    }

    public void OnHeal()
    {
        float value = 30;
        _playerHealth.Heal(value);
        playerUI.Health();
    }

    public void OnDeath(Vector3 position, Vector3 force, GameObject attacker)
    {
        if (!photonView.IsMine) return;
       
        DropStar();
        starCountOnDeath();
        Died = true;
        characterLocomotion.TryStartAbility(dropAbility);
        characterLocomotion.TryStartAbility(dieAbility);
        DropItems();
        itemEquip.OnDeath();
        myStars.OnDeath();
        id = 0;

    }

    public void OnRespawn()
    {
        Died = false;
        playerUI.Health();
    }

    /// <summary>
    /// Drops everything from the inventory 
    /// </summary>
    private void DropItems()
    {
        var inventory = GetComponent<InventoryBase>();
        if (inventory == null)
        {
            return;
        }

        for (int i = 0; i < itemTypes.Count; i++)
        {
           
            var amt = inventory.GetItemIdentifierAmount(itemTypes[i]);
            inventory.RemoveItem(itemTypes[i], i, amt, true);
           
        }
    }

    public void DropStar()
    {
        var inventory = GetComponent<InventoryBase>();
        if (inventory == null)
        {
            return;
        }
        var amt = myStars._myStarCount;
     
        for (int i = 0; i < amt; ++i)
        {    
               
              inventory.RemoveItem(m_ItemTypeStar, i, amt, true);
              Vector3 offset = new Vector3(UnityEngine.Random.Range(-5, 5), 1, 0);
              PhotonNetwork.Instantiate(StarPickup, transform.position + offset, transform.rotation);
        }


    }

    public void StarCount()
    {
        int a = 1;
        float teamcode;
        if (MyPlayerManager.Instance.TeamCode == 1)
        {
            teamcode = 1;
        }
        else
        {
            teamcode = 2;
        }

        StarManager.Instance.StarCount(a, teamcode);
        myStars.StarCount();
    }

    public void starCountOnDeath()
    {
        int amt = myStars._myStarCount;
        float teamcode;
        if (MyPlayerManager.Instance.TeamCode == 1)
        {
            teamcode = 1;
        }
        else
        {
            teamcode = 2;
        }
       

        StarManager.Instance.StarCountOnDeath(amt, teamcode);
    }

  
}
