﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.UI;
using Photon.Pun.UtilityScripts;

public class StartWaitngRoom : MonoBehaviourPunCallbacks
{
   
    private bool _readytoStart;
    private bool startinggame;
    public Text _startTimerText;
    public Text _playerCount;
    private float _startTimer;
    PhotonView pv;
    [Tooltip("The name of the scene to load when all player has joined a room.")]
    [SerializeField] protected string m_SceneName = "DemoRoom";
    // Start is called before the first frame update
    private void Start()
    {
        _startTimer = 5;
        _readytoStart = false;
        startinggame = true;
        pv = GetComponent<PhotonView>();
        _startTimerText.enabled = false;
       
        PlayerCount();
    }

    // Update is called once per frame
    void LateUpdate()
    {
        WaitingforAllPlayers();

    }
    private void PlayerCount()
    {
        JoinTeam();
       // _playerCount.text = PhotonNetwork.CurrentRoom.PlayerCount.ToString() + "/" + PhotonNetwork.CurrentRoom.MaxPlayers; ;
        if (PhotonNetwork.CurrentRoom.PlayerCount == 2)
        {
            _readytoStart = true;
        }
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        PlayerCount();
        
    }

    public void WaitingforAllPlayers()
    {
        if (_readytoStart == true)
        {
            _startTimer -= Time.deltaTime;
            _startTimerText.enabled = true;
            _startTimerText.text =  _startTimer.ToString("f0");
        }

        if (_startTimer <= 0f)
        {
            if (startinggame == true)
                _readytoStart = false;
               
            StartCoroutine(StartGame());
        }
    }

    public IEnumerator StartGame()
    {
        yield return new WaitForSeconds(0.5f);
        if (startinggame == true)
        {
            startinggame = false;

          
            PhotonNetwork.CurrentRoom.IsOpen = false;
            PhotonNetwork.LoadLevel(m_SceneName);
        }
    }
    public void JoinTeam()
    {

        string blue = "Blue";
        string red = "Red";


        if (PhotonTeamsManager.Instance.GetTeamMembersCount(blue) > PhotonTeamsManager.Instance.GetTeamMembersCount(red))
        {
            PhotonNetwork.LocalPlayer.JoinTeam(red);


            Debug.Log("Joined Red");
        }
        else if (PhotonTeamsManager.Instance.GetTeamMembersCount(red) > PhotonTeamsManager.Instance.GetTeamMembersCount(blue))
        {
            PhotonNetwork.LocalPlayer.JoinTeam(blue);


            Debug.Log("Joined Blue");
        }
        else if (PhotonTeamsManager.Instance.GetTeamMembersCount(red) == PhotonTeamsManager.Instance.GetTeamMembersCount(blue))
        {
            PhotonNetwork.LocalPlayer.JoinTeam(blue);


            Debug.Log("Joined BlueDefault");
        }


    }

}
