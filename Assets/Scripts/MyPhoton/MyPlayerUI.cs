﻿
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun.UtilityScripts;
using Opsive.UltimateCharacterController.Traits;
using Photon.Pun;

public class MyPlayerUI : MonoBehaviour
{

    [Tooltip("UI Text to display Player's Name")]
    [SerializeField]
    private Text playerNameText;


    [Tooltip("UI Slider to display Player's Health")]
    [SerializeField]
    private Image playerHealthSlider;

    [Tooltip("UI Image to display Player's Team")]
    [SerializeField] public Image playerTeam;

    [SerializeField] public float _health;

    [SerializeField] public float _totalHealth = 100;


    private Transform MainCam;
    private PhotonTeam photonTeam;
    public PhotonView photonView;


    public Canvas Canvas;
    public Health characterHealth;
    public AttributeManager attributeManager;
    public HealthFlash healthFlash;
    private MyPlayerManager target;


    void Awake()
    {
     
        characterHealth = transform.parent.parent.GetComponent<CharacterHealth>(); ;
        attributeManager = transform.root.GetComponent<AttributeManager>();
        
    }

    private void Start()
    {
        photonView = transform.root.GetComponent<PhotonView>();
        healthFlash = FindObjectOfType<HealthFlash>();
        MainCam = GameObject.FindGameObjectWithTag("MainCamera").transform;
      
    }
    public void SetTarget(MyPlayerManager _target)
    {
        if (_target == null)
        {
            Debug.LogError("<Color=Red><a>Missing</a></Color> PlayMakerManager target for PlayerUI.SetTarget.", this);
            return;
        }
       
        // Cache references for efficiency
        target = _target;
        
      
        if (playerNameText != null)
        {
            playerNameText.text = target.photonView.Owner.NickName;
          
        }
       

      
       
    }
    private void GetTeam()
    {
        if (!photonView.IsMine) return;

       
        if (MyGameManager.Instance.TeamCode == 1)
        {
            playerTeam.color = Color.blue;
        }
        else if(MyGameManager.Instance.TeamCode == 2)
        {
            playerTeam.color = Color.red;
        }
    }

    public void Health()
    {
        _health = characterHealth.HealthValue;
        playerHealthSlider.fillAmount = _health / _totalHealth;
        if (!photonView.IsMine) return;
        // DamageUI(_health);
    }
    private void DamageUI(float a)
    {
        // healthFlash.GetComponent<Image>().enabled = true;
        healthFlash.DamageUI(a, _totalHealth);
    }

}
