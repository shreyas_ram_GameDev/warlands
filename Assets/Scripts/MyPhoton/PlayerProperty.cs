﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerProperty

{
    public const string PLAYER_READY = "IsPlayerReady";
    public const string PLAYER_COLOR = "TeamCode";
    public const string PLAYER_PHOTON_ID = "PhotonID";
    public const string Is_PlayerLeader = "PlayerLeader";
}
  
