﻿using Photon.Pun;
using Photon.Realtime;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using ExitGames.Client.Photon;
using Hashtable = ExitGames.Client.Photon.Hashtable;
using System;
using Photon.Pun.UtilityScripts;
using System.Collections;

public class GameNetwork : MonoBehaviourPunCallbacks
{
    [Tooltip("Should the game automatically connect to a room?")]
    [SerializeField] protected bool m_AutoConnect;
    [Tooltip("The maximum number of players that can join a single room.")]
    [SerializeField] protected int m_MaxPlayerCount = 8;
    [Tooltip("The button that starts connecting to a room.")]
    [SerializeField] protected Button m_ConnectButton;
    [Tooltip("Add all bots button.")]
    [SerializeField] protected Button addbots;
    [Tooltip("UI showing the status of the connection.")]
    [SerializeField] protected Text m_Status;
    [Tooltip("The name of the scene to load when the player has joined a room.")]
    [SerializeField] protected string m_SceneName = "DemoRoom";
    [Tooltip("The name of the scene to load when the player leaves a  room.")]
    [SerializeField] protected string m_MainMenuSceneName = "MainMenu";
    [Tooltip("The toggle that switches between a first and third person start.")]
    [SerializeField] protected Toggle m_PerspectiveToggle;


    private bool m_IsConnecting;

    [SerializeField] protected float _timesinceLastPlayerjoined;

    [SerializeField] protected int _aLLCount;
    [SerializeField] protected int _curentLevel = 0;
    [SerializeField] protected float _botAddTime = 0;

    public bool isTesting;
    private byte blue = 1;
    private byte red = 2;

    public static event Action OnSceneUnLoaded;
    public GameObject WonPlanel;
    private Canvas Canvas;

    /// <summary>
    /// Initialize the default values.
    /// </summary>
    private void Awake()
    {
        UnityEngine.SceneManagement.SceneManager.sceneLoaded += OnSceneLoaded;
        DontDestroyOnLoad(this.gameObject);
    }

    private void Start()
    {
      //  WonPlanel.SetActive(false);
       if (PhotonNetwork.IsMasterClient)
       {
          _timesinceLastPlayerjoined = 0;
       }


    }

    private void Update()
    {
        if (_curentLevel == 1 && PhotonNetwork.IsMasterClient && _aLLCount < PhotonNetwork.CurrentRoom.MaxPlayers)
        {
            if (_aLLCount <= PhotonNetwork.CurrentRoom.MaxPlayers)
            {
                BotCount();
            }
        }
    }
    /// <summary>
    /// On Main Game Scene Loaded
    /// </summary>
    void OnSceneLoaded(Scene scene, LoadSceneMode loadingMode)
    {
        if (scene.buildIndex == 1)
        {
            this.CalledOnLevelWasLoaded(1);
        }
    }

    void CalledOnLevelWasLoaded(int level)
    {
        _curentLevel = level;
        if (PhotonNetwork.IsMasterClient)
        {
            AllCount();
            MyGameManager.Instance.AllPlayerCounts = _aLLCount;
            addbots = UIManager.Instance._addbots;
            addbots.onClick.AddListener(() =>
            {
                _botAddTime = 6;
            });
        }
    }

    #region Bot
    /// <summary>
    /// Keeps the counts of bots and player 
    /// </summary>
    public int AllCount()
    {
        _aLLCount = PhotonNetwork.CurrentRoom.PlayerCount + BotManager.Instance._botCount;
       
        if (PhotonNetwork.IsMasterClient)
        {
            if (_aLLCount == PhotonNetwork.CurrentRoom.MaxPlayers)
            {
                PhotonNetwork.CurrentRoom.IsOpen = false;
            }

            MyGameManager.Instance.AllCountInc();
            if (MyGameManager.Instance.AllPlayerCounts == PhotonNetwork.CurrentRoom.MaxPlayers)
            {
                StartCoroutine(RpcBotTeamCheck());
                
            }
        }
        return _aLLCount;
    }

    public IEnumerator RpcBotTeamCheck()
    {
        yield return new WaitForSeconds(0.2f);
        BotManager.Instance.RpcTeamCheck();
       
    }
    /// <summary>
    ///Adds bots to the game when required
    /// </summary>
    private void BotCount()
    {
        _timesinceLastPlayerjoined += Time.deltaTime;
      
      
        if (_timesinceLastPlayerjoined >= _botAddTime)
        {

            AddPlayerBot();
            AllCount();
            _timesinceLastPlayerjoined = 5;
            if (MyGameManager.Instance.AllPlayerCounts == PhotonNetwork.CurrentRoom.MaxPlayers)
            {
                MyGameManager.Instance.CheckPlayersReady();
            }

        }
    }

    /// <summary>
    /// Main function which uses BotManager to add bots
    /// </summary>
    private void AddPlayerBot()
    {

        MyGameManager.Instance.AllPlayerCounts++;
        BotManager.Instance.AddBot(1);
    }

    /// <summary>
    /// Button to Connect to online game
    /// </summary>
    public void Connect(bool singlePlayer)
    {
        if (singlePlayer == true)
        {
            m_MaxPlayerCount = 1;
        }
        if (m_IsConnecting)
        {
            return;
        }

        if (PhotonNetwork.IsConnected)
        {
            PhotonNetwork.JoinRandomRoom();
            // SetStatus("Joining an existing Lobby.");
            SetStatus("Joining an existing room.");

        }
        else
        {



            // PhotonNetwork.AutomaticallySyncScene = true;

            PhotonNetwork.GameVersion = Opsive.UltimateCharacterController.Utility.AssetInfo.Version;
            PhotonNetwork.ConnectUsingSettings();
            SetStatus("Connecting to Photon Network");
        }

        m_IsConnecting = true;
        if (m_ConnectButton != null)
        {
            m_ConnectButton.interactable = false;
        }
        if (m_PerspectiveToggle != null)
        {
            m_PerspectiveToggle.interactable = false;
        }
    }

    #endregion

    #region PhotonCalbacks
    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        _timesinceLastPlayerjoined = -50;
         MyGameManager.Instance.AllPlayerCounts++;
        _aLLCount++;
        UIManager.Instance.PlayerCount();
        if (MyGameManager.Instance.AllPlayerCounts == PhotonNetwork.CurrentRoom.MaxPlayers)
        {
           
            PhotonNetwork.CurrentRoom.IsOpen = false;
        }
         
    }
  



    /// <summary>
    /// The player has connected to the Photon Network.
    /// </summary>
    public override void OnConnectedToMaster()
    {
        if (m_IsConnecting)
        {
            PhotonNetwork.JoinRandomRoom();
            //PhotonNetwork.AutomaticallySyncScene = true;
            SetStatus("Joining an existing room.");
        }


    }

    /// <summary>
    /// There are no rooms available. Create a new room.
    /// </summary>

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        var roomOptions = new RoomOptions() { IsVisible = true, IsOpen = true };
        roomOptions.MaxPlayers = (byte)m_MaxPlayerCount;
        PhotonNetwork.CreateRoom(null, roomOptions);

        SetStatus("No rooms available. Creating a room.");
    }

    /// <summary>
    /// The player has joined a room. Load the game scene.
    /// </summary>
    /// 

    public override void OnJoinedRoom()
    {

        // JoinTeam();
        SceneManager.LoadScene(m_SceneName);
        SetStatus("Waiting for other Players.");


       
        if (isTesting == true)
        {
            PhotonNetwork.CurrentRoom.MaxPlayers = 1;
        }
      
       
            PhotonNetwork.LoadLevel(m_SceneName);
            SetStatus("Waiting for other Players.");
       



    }
    /// <summary>
    /// Sets the status text to the specified value.
    /// </summary>
    /// <param name="status">The status text value.</param>
    /// 
    #endregion

    public IEnumerator Disconnect()
    {
        yield return new WaitForSeconds(0.5f);
        WonPlanel.SetActive(true);
        Canvas = GameObject.Find("MainMenuCanvas").GetComponent<Canvas>();
        GameObject won = Instantiate(WonPlanel);
        won.transform.SetParent(Canvas.GetComponent<Transform>(), false);
        PhotonNetwork.Disconnect();
    }


    /// <summary>
    /// Starts the game
    /// </summary>
    [PunRPC]
    private void StartGame()
    {
        if (!PhotonNetwork.IsMasterClient)
            return;
        PhotonNetwork.CurrentRoom.IsOpen = false;
        PhotonNetwork.LoadLevel(m_SceneName);
    }
    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        if (PhotonTeamsManager.Instance.GetTeamMembersCount(blue) == 0 || PhotonTeamsManager.Instance.GetTeamMembersCount(red) == 0)
        {
            PhotonNetwork.LoadLevel(m_MainMenuSceneName);
            Debug.Log("LeftRoom");
            OnSceneUnLoaded();
            OnAllplayersLeftTeam();
            StartCoroutine(Disconnect());


        }
    }
    public void OnAllplayersLeftTeam()
    {
        if (PhotonTeamsManager.Instance.GetTeamMembersCount(blue) == 0)
        {
            Debug.Log("TeamRedWon");
        }
        else if (PhotonTeamsManager.Instance.GetTeamMembersCount(red) == 0)
        {
            Debug.Log("TeamBlueWon");
        }
    }


    /// <summary>
    /// Ui which shows status of the connection
    /// </summary>
    private void SetStatus(string status)
    {
        if (m_Status == null)
        {
            return;
        }

        m_Status.text = status;
    }

    public void LeaveLobby()
    {
        SceneManager.LoadScene(m_MainMenuSceneName);

        PhotonNetwork.Disconnect();

    }

    private void OnDestroy()
    {
        UnityEngine.SceneManagement.SceneManager.sceneLoaded -= OnSceneLoaded;
    }

}



