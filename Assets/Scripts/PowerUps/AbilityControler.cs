﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;

public class AbilityControler : MonoBehaviour
{
    public ParticleSystem _text1;
    public ParticleSystem _text2;
    public ParticleSystem _Effect;


    public Button[] _abilityButtons;
    public Sprite[] _abilitySprites;
    public Sprite _sprite;



    public void UpdateAblityUI(string _landType)
    {

        PlayEffect();
        for (int i = 0; i < _abilityButtons.Length; i++)
        {
            if (_abilityButtons[i].gameObject.activeSelf == false)
            {
                SetButtonSprite(_landType);
                _abilityButtons[i].gameObject.SetActive(true);
                _abilityButtons[i].GetComponent<Image>().sprite = _sprite;
                return;

            }
        }
    }
    public void SetButtonSprite(string abilityType)
    {
        foreach (var sprite in _abilitySprites)
        {
            
            if (sprite.name == abilityType)
            {
                _sprite = sprite;
            }

        }
    }

    public void PlayEffect()
    {
        Debug.Log("Play");
        _text1.Play();
        _text2.Play();
        _Effect.Play();
       
    }
}
