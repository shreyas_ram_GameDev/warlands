﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

public class AblityHandler : MonoBehaviourPunCallbacks
{
    public static AblityHandler Instance;
    public AbilityControler abilityControler;

    private void Start()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }
    public void UpdateAbility(float teamcode, string abilityType)
    {
        if (!photonView.IsMine) return;
        photonView.RPC("ActivateAblity", RpcTarget.All, teamcode, abilityType);
        photonView.RPC("ACtivateAreaMM", RpcTarget.All, teamcode, abilityType);

    }

    [PunRPC]
    public void ActivateAblity(float teamcode , string abilityType)
    {

        if (MyPlayerManager.Instance.TeamCode != teamcode) return;      
        abilityControler.UpdateAblityUI(abilityType);
    }

    [PunRPC]
    public void ACtivateAreaMM(float teamcode, string abilityType)
    {

        MiniMapHandler.Instance.UnlockArea(teamcode, abilityType);
    }
    
    
}
