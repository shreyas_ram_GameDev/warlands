using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Opsive.UltimateCharacterController.Character;
using Opsive.UltimateCharacterController.Character.Abilities;

public class HidingBehaviour : MonoBehaviour
{
    private UltimateCharacterLocomotion _characterLocomotion;
    private HeightChange heightChangeAbility;
    private void Awake() {
        _characterLocomotion = this.GetComponent<UltimateCharacterLocomotion>();
        
    }
    void Start()
    {
        heightChangeAbility = _characterLocomotion.GetAbility<HeightChange>();
        HidingArea.isPlayerHiding += HandlePlayerHiding;
    }
    private void OnDisable() {
        HidingArea.isPlayerHiding -= HandlePlayerHiding;
    }

    private void HandlePlayerHiding(bool isPlayerHiding)
    {
        if(isPlayerHiding)
        {
            Debug.Log("isHiding");
            _characterLocomotion.TryStartAbility(heightChangeAbility, true);
        }
        else
        {
            _characterLocomotion.TryStopAbility(heightChangeAbility, true);
        }
    }
}
