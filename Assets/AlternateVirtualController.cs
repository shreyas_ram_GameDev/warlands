using Opsive.Shared.Events;
using Opsive.Shared.Input.VirtualControls;
using UnityEngine;

public class AlternateVirtualController : VirtualControlsManager
{
    protected override void Awake()
    {
        base.Awake();

        if (m_Character == null) {
            // EventHandler.ExecuteEvent(m_Character, "OnEnableGameplayInput", false);
            EventHandler.RegisterEvent("OnAttachCharacter", OnAttachCharacter);
        }
    }

    private void OnAttachCharacter()
    {
        
        Character = GameObject.Find("BlueDragon").gameObject;
        
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();

        EventHandler.RegisterEvent("OnAttachCharacter", OnAttachCharacter);
    }
}
